import React from 'react';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

import SignIn from '../../routes/SignIn';
import SignUp from '../../routes/SignUp/index';
import Header from '../../routes/Header/index';
import Footer from '../../routes/Footer/index';
import Dashboard from '../../routes/Dashboard/index';
import RideSchedule from '../../routes/RideSchedule/index';
import ForgotPassword from '../../routes/ForgotPassword/index';
import ForgotPasswordResponse from '../../routes/ForgotPasswordResponse/index';
import ViewProfile from '../../routes/ViewProfile/index';
import RideHistory from '../../routes/RideHistory/index';
import Support from '../../routes/Support/index';
import ParentAndChildrenProfiles from '../../routes/ParentAndChildrenProfiles/index';
import TermsOfService from '../../routes/SignUp/termsofservice';
import ManageAccount from '../../routes/ManageAccount/index';

//injectTapEventPlugin();

const App = () => {
    return (
        <BrowserRouter>
            <Header />
            <Switch>
                <PrivateRoute exact path="/viewprofile" component={ViewProfile} />
                <Route exact path="/manageaccount" component={ManageAccount} />
                <Route exact path="/termsofservice" component={TermsOfService} />
                <Route exact path="/parentchildrenprofiles" component={ParentAndChildrenProfiles} />
                <Route exact path="/support" component={Support} />
                <Route exact path="/ridehistory" component={RideHistory} />
                <Route exact path="/forgotpasswordresponse" component={ForgotPasswordResponse} />
                <Route exact path="/forgotpassword" component={ForgotPassword} />
                <Route exact path="/rideschedule" component={RideSchedule} />
                <Route exact path="/dashboard" component={Dashboard} />
                <Route exact path="/signup" component={SignUp} />
                <Route exact path="/login" component={SignIn} />
                <Redirect to="/login" />
            </Switch>
            <Footer />
        </BrowserRouter>
    );
}


const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            (localStorage.getItem("parent_id") !== null) ?
                (<Component {...props} />)
                :
                (<Redirect to={{ pathname: "/login" }} />)
        }
    />
);

export default App;
