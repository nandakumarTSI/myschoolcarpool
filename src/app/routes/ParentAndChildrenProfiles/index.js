import React from 'react';

import ParentAndChildrenProfiles from '../../components/ParentAndChildrenProfiles/index';

const parentAndChildrenProfiles = () => <ParentAndChildrenProfiles />;

export default parentAndChildrenProfiles;
