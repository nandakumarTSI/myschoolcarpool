import React from 'react';

import ForgotPasswordResponse from '../../components/ForgotPasswordResponse/index';

const forgotPasswordResponse = () => <ForgotPasswordResponse />;

export default forgotPasswordResponse;
