import { GET_RIDE_HISTORY } from '../actions/types/index';

const initialState = {
    data: {},
};

export default function getRideHistory(state = initialState, action = {}) {
    switch (action.type) {
        case GET_RIDE_HISTORY:
            return {
                data: action.data,
            };
        default:
            return state;
    }
};
