import { SET_CURRENT_USER } from '../actions/types/index';

const initialState = {
  user: {},
};

export default function login(state = initialState, action = {}) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        user: action.user,
      };
    default:
      return state;
  }
};
