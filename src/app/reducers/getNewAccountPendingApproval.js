import { GET_NEW_ACCOUNT_PENDING_APPROVAL } from '../actions/types/index';

const initialState = {
    data: {},
};

export default function getNewAccountPendingApproval(state = initialState, action = {}) {
    switch (action.type) {
        case GET_NEW_ACCOUNT_PENDING_APPROVAL:
            return {
                data: action.data,
            };
        default:
            return state;
    }
};
