import { GET_RECENT_HISTORY } from '../actions/types/index';

const initialState = {
    data: {},
};

export default function getRecentHistory(state = initialState, action = {}) {
    switch (action.type) {
        case GET_RECENT_HISTORY:
            return {
                data: action.data,
            };
        default:
            return state;
    }
};
