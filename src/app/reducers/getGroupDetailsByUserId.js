import { GET_GROUP_DETAILS_BY_USER_ID } from '../actions/types/index';

const initialState = {
  data: {},
};

export default function getGroupDetailsByUserId(state = initialState, action = {}) {
  switch (action.type) {
    case GET_GROUP_DETAILS_BY_USER_ID:
      return {
        data: action.data,
      };
    default:
      return state;
  }
};
