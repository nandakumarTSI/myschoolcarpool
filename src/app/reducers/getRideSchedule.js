import { GET_GROUP_RIDE_ALL } from '../actions/types/index';

const initialState = {
    data: {},
};

export default function getGroupRidesAll(state = initialState, action = {}) {
    switch (action.type) {
        case GET_GROUP_RIDE_ALL:
            return {
                data: action.data,
            };
        default:
            return state;
    }
};
