import { RESET_PASSWORD } from '../actions/types/index';

const initialState = {
  user: {},
};

export default function resetpassword(state = initialState, action = {}) {
  switch (action.type) {
    case RESET_PASSWORD:
      return {
        user: action.user,
      };
    default:
      return state;
  }
};
