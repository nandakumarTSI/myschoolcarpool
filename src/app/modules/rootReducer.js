
import { combineReducers } from 'redux';
import login from '../reducers/login';
import resetpassword from '../reducers/resetpassword';
import getGroupDetailByUserId from '../reducers/getGroupDetailsByUserId';
import getNewAccountPendingApproval from '../reducers/getNewAccountPendingApproval';
import getGroupRidesAll from '../reducers/getRideSchedule';
import getRideHistory from '../reducers/getRideHistory';
import getRecentHistory from '../reducers/getRecentHistory';

const appReducer = combineReducers({
  login,
  resetpassword,
  getGroupDetailByUserId,
  getNewAccountPendingApproval,
  getGroupRidesAll,
  getRideHistory,
  getRecentHistory,
});

export default (state, action) => {
  if (action.type === "LOGOUT_SUCCESS") {
    Object.keys(state).map(el => {
      if (el !== "ui") { // Add your non reset redux state here
        state[el] = undefined;
      }
      return state[el];
    });
  }
  return appReducer(state, action);
}
