import React, { Component } from 'react';
//import './style.css';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import RecentHistory from '../RecentHistory/index';
import UserDetails from '../UserDetails/index';
import { createGetGroupDetailsRequest } from '../../actions/GetGroupDetailsByUserIdAction';
import { createGetNewAccountPendingApprovalRequest } from '../../actions/GetNewAccountPendingApprovalAction';
import Loader from '../Loader/index';

class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            group_id: 0,
            groupErrorMsg: '',
            newAccErrorMsg: '',
            isLoading: true
        }

    }

    componentDidMount() {
        this.props.createGetGroupDetailsRequest()
            .then(
                res => {
                    if (res.data.status === true) {
                        if (res.data.body.length > 0) {
                            this.setState({
                                group_id: res.data.body[0].id,
                                groupErrorMsg: ''
                            });
                            localStorage.setItem("group_id", res.data.body[0].id);
                            this.props.createGetNewAccountPendingApprovalRequest(res.data.body[0].id)
                                .then(
                                    res => {
                                        if (res.data.status === true) {
                                            this.setState({
                                                newAccErrorMsg: '',
                                                isLoading: false
                                            });
                                        }
                                        else {
                                            const msg = (!isEmpty(res.data.error_code) ? "Error Code: " + res.data.error_code : "") + " " + (!isEmpty(res.data.message) ? "Message: " + res.data.message : "");
                                            this.setState({
                                                groupErrorMsg: '',
                                                newAccErrorMsg: msg,
                                                isLoading: false
                                            });
                                        }
                                    },
                                    err => {
                                        this.setState({
                                            groupErrorMsg: '',
                                            newAccErrorMsg: err.toString(),
                                            isLoading: false
                                        });
                                    }
                                )
                        }
                        else {
                            this.setState({
                                group_id: 0,
                                groupErrorMsg: "No Group Details Found",
                                newAccErrorMsg: '',
                                isLoading: false
                            });
                        }
                    }
                    else {
                        const msg = (!isEmpty(res.data.error_code) ? "Error Code: " + res.data.error_code : "") + " " + (!isEmpty(res.data.message) ? "Message: " + res.data.message : "");
                        this.setState({
                            group_id: 0,
                            groupErrorMsg: msg,
                            newAccErrorMsg: '',
                            isLoading: false
                        });
                    }
                },
                err => {
                    this.setState({
                        group_id: 0,
                        groupErrorMsg: err.toString(),
                        newAccErrorMsg: '',
                        isLoading: false
                    });
                }
            )
    }

    handleNewPendingApproval = (GroupId) => {
        localStorage.setItem("group_id", GroupId);
        this.setState({
            isLoading: true
        });
        this.props.createGetNewAccountPendingApprovalRequest(GroupId)
            .then(
                res => {
                    if (res.data.status === true) {

                        <UserDetails details={res.data.body.group.parents} />

                        this.setState({
                            newAccErrorMsg: ''
                        });

                    } else {
                        const msg = (!isEmpty(res.data.error_code) ? "Error Code: " + res.data.error_code : "") + " " + (!isEmpty(res.data.message) ? "Message: " + res.data.message : "");
                        this.setState({
                            newAccErrorMsg: msg
                        });
                    }
                },
                err => {
                    this.setState({
                        newAccErrorMsg: err.toString()
                    });
                }
            )
            .then(x => {
                this.setState({
                    isLoading: false
                });
            });
    }

    render() {
        const { groupDetailByUserId, getNewAccountPendingApproval } = this.props;
        const isLoading = this.state.isLoading;

        if (isLoading) {
            return <Loader />
        }
        else {
            return (
                <div className='container-recent-history-div'>
                    <div className='row'>
                        <RecentHistory />
                        <div className='col-md-9 p-0'>
                            <div className='capsule'>
                                <div className='title-color'>DASHBOARD</div>
                                <div className='account-summary'>
                                    <div className='account-summary-title'>Account Summary</div>
                                    <div className='account-summary-hr'></div>
                                    {
                                        (Object.keys(groupDetailByUserId).length > 0 && groupDetailByUserId.body !== undefined && Object.keys(groupDetailByUserId.body).length > 0 && this.state.groupErrorMsg === '')
                                            ?
                                            groupDetailByUserId.body.map((response) => {
                                                return <div className='group-name-container' key={response.id} onClick={() => this.handleNewPendingApproval(response.id)}>
                                                    <div className='group-name-title'>Group Name: {response.group_name}</div>
                                                    <div className='group-name-counts'>
                                                        <div className='parents-count'>{response.parents_count} Parents</div>|
                                                        <div className='drivers-count'>{response.driver_count} Drivers</div>|
                                                        <div className='children-count'>{response.student_count} Children</div>
                                                    </div>
                                                </div>
                                            })
                                            :
                                            <div className='warn-msg'>{this.state.groupErrorMsg !== '' ? this.state.groupErrorMsg : 'No Group Details Found'}</div>
                                    }
                                </div>
                                <div className='new-acc-pending'>
                                    <div className='new-acc-pending-title'>New Accounts Pending Approval</div>
                                    <div className='account-summary-hr'></div>
                                    {
                                        (Object.keys(getNewAccountPendingApproval).length > 0 && getNewAccountPendingApproval.body !== undefined && getNewAccountPendingApproval.body.group !== undefined && Object.keys(getNewAccountPendingApproval.body.group).length > 0 && this.state.newAccErrorMsg === '')
                                            ?
                                            <UserDetails details={getNewAccountPendingApproval.body.group.parents} />
                                            :
                                            <div className='warn-msg'>{this.state.newAccErrorMsg !== '' ? this.state.newAccErrorMsg : 'No New Accounts Pending Approval'}</div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    const { login, getGroupDetailByUserId, getNewAccountPendingApproval } = state;
    return {
        user: login.user,
        groupDetailByUserId: getGroupDetailByUserId.data,
        getNewAccountPendingApproval: getNewAccountPendingApproval.data
    }
}
export default connect(mapStateToProps, { createGetGroupDetailsRequest, createGetNewAccountPendingApprovalRequest })(Dashboard);