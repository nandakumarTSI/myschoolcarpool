import { toast } from 'react-toastify';
toast.configure();

export function Warning(msg) {
    return toast.warning(msg, {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        pauseOnFocusLoss: true,
        style: {
            width: "100%"
        }
    });
}

export function Success(msg) {
    return toast.success(msg, {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        pauseOnFocusLoss: true,
        style: {
            width: "100%"
        }
    });
}

export function Error(msg) {
    return toast.error(msg, {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        pauseOnFocusLoss: true,
        style: {
            width: "100%"
        }
    });
}

export function Info(msg) {
    return toast.info(msg, {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        pauseOnFocusLoss: true,
        style: {
            width: "100%"
        }
    });
}