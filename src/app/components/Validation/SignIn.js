import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};
    
    if (Validator.isEmpty(data.email)) {
        errors.emailErrorMsg = "Please enter email address.";
    }
    else if (!Validator.isEmpty(data.email) && !Validator.isEmail(data.email)) {
        errors.emailErrorMsg = "Please enter valid email address.";
    }
    else {
        errors.emailErrorMsg = "";
    }

    if (Validator.isEmpty(data.password)) {
        errors.passwordErrorMsg = "Please enter password";
    }
    else {
        errors.passwordErrorMsg = "";
    }

    if (Validator.isEmpty(errors.emailErrorMsg) && Validator.isEmpty(errors.passwordErrorMsg)) {
        errors = {};
    }

    return {
        errors,
        isValid: isEmpty(errors),
    };
}
