import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
  let errors = {};

  if (isEmpty(data.your_name)) {
    errors.yourNameErrMsg = "Please fill Your Name field";
  }
  else {
    errors.yourNameErrMsg = "";
  }

  if (isEmpty(data.your_email)) {
    errors.replyEmailErrMsg = "Please fill Reply Email field";
  }
  else if (!Validator.isEmail(data.your_email)) {
    errors.replyEmailErrMsg = "Please enter valid email address";
  }
  else {
    errors.replyEmailErrMsg = "";
  }

  if (!isEmpty(data.your_phone) && data.your_phone.length < 10) {
    errors.yourPhoneErrMsg = "Please enter valid email address";
  }
  else {
    errors.yourPhoneErrMsg = "";
  }

  if (isEmpty(data.your_message)) {
    errors.messageErrMsg = "Please fill Message field";
  }
  else {
    errors.messageErrMsg = "";
  }

  if (isEmpty(errors.yourNameErrMsg) && isEmpty(errors.replyEmailErrMsg) && isEmpty(errors.messageErrMsg) && isEmpty(errors.alternatePhoneErrorMsg)) {
    errors = {};
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
}
