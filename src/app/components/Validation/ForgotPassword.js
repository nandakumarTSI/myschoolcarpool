import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};
    
    if (Validator.isEmpty(data.email)) {
        errors.emailErrorMsg = "Please enter email address.";
    }
    else if (!Validator.isEmpty(data.email) && !Validator.isEmail(data.email)) {
        errors.emailErrorMsg = "Please enter valid email address.";
    }
    else {
        errors.emailErrorMsg = "";
    }

    if (Validator.isEmpty(errors.emailErrorMsg)) {
        errors = {};
    }

    return {
        errors,
        isValid: isEmpty(errors),
    };
}
