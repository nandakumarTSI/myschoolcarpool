import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};

    if (isEmpty(data.first_name)) {
        errors.firtNameErrorMsg = "Please enter your first name";
    }
    else {
        errors.firtNameErrorMsg = "";
    }

    if (isEmpty(data.last_name)) {
        errors.lastNameErrorMsg = "Please enter your last name";
    }
    else {
        errors.lastNameErrorMsg = "";
    }

    if (isEmpty(data.phone)) {
        errors.phoneErrorMsg = "Please enter your phone number";
    }
    else if (data.phone.length < 10) {
        errors.phoneErrorMsg = "Please enter valid phone number";
    }
    else {
        errors.phoneErrorMsg = "";
    }

    if (isEmpty(data.email)) {
        errors.emailErrorMsg = "Please enter your email address.";
    }
    else if (!Validator.isEmail(data.email)) {
        errors.emailErrorMsg = "Please enter valid email address.";
    }
    else {
        errors.emailErrorMsg = "";
    }

    if (isEmpty(data.address)) {
        errors.addressErrorMsg = "Please enter your address";
    }
    else {
        errors.addressErrorMsg = "";
    }

    if (isEmpty(data.city)) {
        errors.cityErrorMsg = "Please enter your city";
    }
    else {
        errors.cityErrorMsg = "";
    }

    if (isEmpty(data._state.toString())) {
        errors.stateErrorMsg = "Please enter your state";
    }
    else {
        errors.stateErrorMsg = "";
    }

    if (isEmpty(data.zip)) {
        errors.zipErrorMsg = "Please enter your zip";
    }
    else if (data.zip.length < 5) {
        errors.zipErrorMsg = "Please enter minimum 5 characters";
    }
    else {
        errors.zipErrorMsg = "";
    }

    if (!isEmpty(data.alternate_email) && !Validator.isEmail(data.alternate_email)) {
        errors.alternateEmailErrorMsg = "Please enter valid email address.";
    }
    else {
        errors.alternateEmailErrorMsg = "";
    }

    if (!isEmpty(data.alternate_phone) && data.alternate_phone.length < 10) {
        errors.alternatePhoneErrorMsg = "Please enter valid phone number.";
    }
    else {
        errors.alternatePhoneErrorMsg = "";
    }

    if (isEmpty(data.password)) {
        errors.passwordErrorMsg = "Please enter password";
    }
    else if (data.password.length < 6) {
        errors.passwordErrorMsg = "Please enter minimum 6 characters";
    }
    else {
        errors.passwordErrorMsg = "";
    }

    if (isEmpty(data.confirm_password)) {
        errors.confirmPasswordErrorMsg = "Please confirm your password";
    }
    else if (!isEmpty(data.password) && !isEmpty(data.confirm_password) && data.password !== data.confirm_password) {
        errors.confirmPasswordErrorMsg = "Password does not match";
    }
    else {
        errors.confirmPasswordErrorMsg = "";
    }

    if (isEmpty(errors.firtNameErrorMsg) && isEmpty(errors.lastNameErrorMsg) && isEmpty(errors.phoneErrorMsg) && isEmpty(errors.emailErrorMsg) && isEmpty(errors.addressErrorMsg) && isEmpty(errors.cityErrorMsg) && isEmpty(errors.stateErrorMsg) && isEmpty(errors.zipErrorMsg) && isEmpty(errors.passwordErrorMsg) && isEmpty(errors.confirmPasswordErrorMsg)&& isEmpty(errors.alternateEmailErrorMsg) && isEmpty(errors.alternatePhoneErrorMsg)) {
        errors = {};
    }

    return {
        errors,
        isValid: isEmpty(errors),
    };
}
