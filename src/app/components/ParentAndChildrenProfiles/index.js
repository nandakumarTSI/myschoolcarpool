import React, { useEffect, useState } from "react";
//import './style.css';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Button } from 'react-bootstrap';
import Loader from '../Loader/index';

import RecentHistory from "../RecentHistory/index";
import { createGetNewAccountPendingApprovalRequest } from '../../actions/GetNewAccountPendingApprovalAction';
import { isEmpty } from "lodash";


const ParentAndChildrenProfiles = (props) => {

    const [isLoading, setIsLoading] = useState(true);
    const [errorMsg, setErrorMsg] = useState("");
    const [parentChildrenInfo, setParentChildrenInfo] = useState([]);


    useEffect(() => {
        props.createGetNewAccountPendingApprovalRequest(localStorage.getItem("group_id"))
            .then(
                res => {
                    if (res.data.status === true) {
                        setErrorMsg("");
                        setParentChildrenInfo(res.data.body.group);
                        setIsLoading(false);
                    }
                    else {
                        if (isEmpty(localStorage.getItem("group_id"))) {
                            setErrorMsg("No Parent and Children Found");
                        }
                        else {
                            const msg = "Error Code: " + res.data.error_code + " " + (res.data.message !== "" ? "Message: " + res.data.message : "");
                            setErrorMsg(msg);
                        }
                        setIsLoading(false);
                    }
                },
                err => {
                    setErrorMsg(err.toString());
                    setIsLoading(false);
                }
            );
    }, [])



    if (isLoading) {
        return <Loader />
    }
    else {
        return (
            <div className='container-recent-history-div'>
                <div className='row'>
                    <RecentHistory />
                    <div className='col-md-9 p-0'>
                        <div className='capsule'>
                            <div className='title-color'>VIEW ALL PARENTS AND CHILDREN ASSIGNED TO GROUP</div>
                            <div className='pcp-capsule'>
                                {
                                    (isEmpty(errorMsg))
                                        ?
                                        <>
                                            <PCPCapsuleInfo status='Drivers' info={parentChildrenInfo.drivers} history={props.history} />
                                            <PCPCapsuleInfo status='Parents' info={parentChildrenInfo} history={props.history} />
                                            <PCPCapsuleInfo status='Children' info={parentChildrenInfo} history={props.history} />
                                        </>
                                        :
                                        <div className='warn-msg'>{errorMsg}</div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { getNewAccountPendingApproval } = state;

    return {
        getAllParentsAndChildrenDetails: getNewAccountPendingApproval.data
    }
}

export default withRouter(connect(mapStateToProps, { createGetNewAccountPendingApprovalRequest })(ParentAndChildrenProfiles));


const PCPCapsuleInfo = (data) => {
    const { info: { students, parents }, info, history, status } = data;

    let style = {};
    let type = 'Children: ';

    const handleViewProfile = (parent_id, driver_id) => {
        localStorage.setItem("parent_id", parent_id);
        localStorage.setItem("driver_id", driver_id);
        history.push({
            pathname: "/viewprofile",
            state: {
                parent_id: parent_id,
                driver_id: driver_id
            }
        });
    }

    if (status === "Drivers") {
        style = {
            color: '#6d4c96',
            borderBottom: '3px solid ',
            marginBottom: '20px'
        };
        return (
            <>
                {!isEmpty(info) &&
                    <>
                        <div className='pcp-title' style={style}>{status}</div>
                        <div className='pcp-user-info'>
                            <img src={info.profile_picture} alt='Profile Avatar'></img>
                            <div className='pcp-user-details'>
                                <span className='pcp-user-name'>{info.name}</span>
                                {
                                    Object.keys(students).map((d) => {
                                        return <span key={students[d].id} className='pcp-user-type'>{type} {students[d].name}</span>
                                    })
                                }
                                <div className='pcp-user-btn-ctrls'>
                                    <Button variant='default' onClick={() => handleViewProfile(info.parent_id, info.id)}>View Profile</Button>
                                    <Button variant='default' onClick={() => window.open("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin")}>Send Email</Button>
                                </div>
                            </div>
                        </div>
                    </>}
            </>
        );
    }
    else if (status === "Parents") {
        style = {
            color: '#d5a806',
            borderBottom: '3px solid',
            marginBottom: '20px'
        };
        return (
            <>
                {(!isEmpty(info) && !isEmpty(info.parents)) &&
                    <>
                        <div className='pcp-title' style={style}>{status}</div>
                        {
                            info.parents.map((p) => {
                                return <div key={p.id} className='pcp-user-info'>
                                    <img src={p.picture} alt='Profile Avatar'></img>
                                    <div className='pcp-user-details'>
                                        <span className='pcp-user-name'>{p.first_name} {p.last_name}</span>
                                        {!isEmpty(p.students) ?
                                            p.students.map((d) => {
                                                return <span key={d.id} className='pcp-user-type'>{type} {d.name}</span>
                                            })
                                            :
                                            <div className='pcp-user-type'>{type} <span className='text-danger'>{'No Children Found'}</span></div>
                                        }
                                        <div className='pcp-user-btn-ctrls'>
                                            <Button variant='default' onClick={() => handleViewProfile(p.id, info.driver_id)}>View Profile</Button>
                                            <Button variant='default' onClick={() => window.open("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin")}>Send Email</Button>
                                        </div>
                                    </div>
                                </div>
                            })
                        }
                    </>}
            </>
        );
    }
    else {
        style = {
            color: '#cf4a79',
            borderBottom: '3px solid',
            marginBottom: '20px'
        };
        type = 'Guardians: ';
        return (
            <>
                {
                    (!isEmpty(info) && !isEmpty(students)) &&
                    <>
                        <div className='pcp-title' style={style}>{status}</div>
                        {
                            Object.keys(students).map((s) => {
                                const _parents = parents.filter(x => x.id === students[s].parent_id);
                                return <div key={students[s].id} className='pcp-user-info'>
                                    <img src={students[s].picture} alt='Profile Avatar'></img>
                                    <div className='pcp-user-details'>
                                        <span className='pcp-user-name'>{students[s].name}</span>
                                        {!isEmpty(_parents)
                                            ?
                                            _parents.map((p) => {
                                                return <span key={p.id} className='pcp-user-type'>{type} {p.first_name} {p.last_name}</span>
                                            })
                                            :
                                            <div className='pcp-user-type'>{type} <span className='text-danger'>{'No Guardians Found'}</span></div>
                                        }
                                        <div className='pcp-user-btn-ctrls'>
                                            <Button variant='default' onClick={() => handleViewProfile(students[s].parent_id, students[s].parent_id)}>View Profile</Button>
                                        </div>
                                    </div>
                                </div>
                            })
                        }
                    </>
                }
            </>
        );
    }
}