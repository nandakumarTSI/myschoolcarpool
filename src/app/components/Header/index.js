import React from 'react';
//import './style.css';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { createLogoutRequest } from "../../actions/LogoutAction";
import ManageAccImg from "../../assets/images/ManageAcc.png";
import CarpoolLogo from "../../assets/images/header-logo.png";

class index extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            subMenu: false
        };
        this.toggleSubMenu = this.toggleSubMenu.bind(this);
        
        if (props.location.pathname === '/' || props.location.pathname === '/login')
            localStorage.clear();

        if (props.location.pathname === '/' || props.location.pathname === '/login' || props.location.pathname === '/signup' || props.location.pathname === '/termsofservice' || props.location.pathname === '/forgotpassword' || props.location.pathname === '/forgotpasswordresponse') {
            document.body.classList.add('login-page');
        }
        else {
            document.body.classList.remove('login-page');
        }

    }

    toggleSubMenu() {
        this.setState({ subMenu: !this.state.subMenu });
    }

    handleManageAcc = () => {
        this.props.history.push('/manageaccount');
    }

    handleLogOut = () => {
        document.body.classList.add('login-page');
        localStorage.clear();
        this.props.createLogoutRequest();
        this.props.history.push('/login');
    }

    render() {
        const _token = localStorage.getItem("token");
        const showSubMenu = (this.state.subMenu) ? "show" : "";
        const currentUserName = localStorage.getItem("CurrentUserName");
        return <header>
            <nav className="navbar navbar-expand-lg navbar-light bg-light main-header nav-container">
                <div className="school-container">
                    <div >
                        {<img src={CarpoolLogo} alt="carpool-logo" className="carpool-logo" />}
                    </div>
                    <div>
                        {!isEmpty(_token) && <div className="navbar-nav">
                            <div className='header-title me-0 me-lg-5'>Group Administration Portal</div><span className="d-none d-lg-block">|</span>
                            <div className='header-login-as mx-0 mx-lg-5'>Logged in as: <b>{currentUserName}</b></div><span className="d-none d-lg-block">|</span>
                            <div className='header-manage-acc ms-0 ms-lg-5' onClick={this.handleManageAcc}>
                                <img src={ManageAccImg} alt="manage_acc_image" className="pe-2" />
                                <span>Manage Account</span>
                            </div>
                            <div className="log-out" onClick={this.handleLogOut}>LogOut</div>
                        </div>}
                    </div>
                </div>
            </nav>

            {!isEmpty(_token) && <nav className="navbar navbar-expand-lg navbar-light bg-light sub-header">
                <div className="school-container">
                    <div></div>
                    <button className="navbar-toggler" type="button" onClick={this.toggleSubMenu}>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className={"collapse navbar-collapse " + showSubMenu}>
                        <div className="navbar-nav me-auto mb-2 mb-lg-0 my-0 nav-menu">
                            <Link to='/dashboard' onClick={this.toggleSubMenu}>Dashboard</Link>
                            <Link to='/rideschedule' onClick={this.toggleSubMenu}>Ride Schedule</Link>
                            <Link to='/ridehistory' onClick={this.toggleSubMenu}>Ride History</Link>
                            <Link to='/parentchildrenprofiles' onClick={this.toggleSubMenu}>Parent & Children Profiles</Link>
                            <Link to='/support' onClick={this.toggleSubMenu}>Support</Link>
                        </div>
                    </div></div>
            </nav>}
        </header>
    }
}

const mapStateToProps = (state) => {
    const { login } = state;
    return {
        user: login.user
    }
}

export default withRouter(connect(mapStateToProps, { createLogoutRequest })(index));