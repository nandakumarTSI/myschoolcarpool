import React, { useState } from "react";
//import './style.css';
import { Button, Form } from 'react-bootstrap';
import isEmpty from 'lodash/isEmpty';

import RecentHistory from '../RecentHistory/index';
import { validateInput } from '../Validation/Support';
import Loader from '../Loader/index';
import { Success, Error } from '../ToastMessage/index';
import config from '../../config';

const Support = () => {
    const [state, setState] = useState({
        your_name: "",
        your_email: "",
        your_phone: "",
        your_message: "",
        isLoading: false
    });

    const [error, setError] = useState({
        your_name_err_msg: "",
        your_email_err_msg: "",
        your_message_err_msg: "",
        your_phone_err_msg: ""
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        if (isValid()) {
            setState({ ...state, isLoading: true });
            fetch(`${config.apiURL}/user/createContactSupport`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "your_name": state.your_name,
                    "your_email": state.your_email,
                    "your_phone": state.your_phone,
                    "your_message": state.your_message
                })
            })
                .then((response) => response.json())
                .then(response => {
                    if (response.status === true) {
                        Success(response.message);
                        setState({ isLoading: false });
                    }
                    else {
                        const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                        Error(msg);
                        setState({ ...state, isLoading: false });
                    }
                })
                .catch(err => {
                    Error(err.toString());
                    setState({ ...state, isLoading: false });
                });
        }
    }

    const isValid = () => {
        const { errors, isValid } = validateInput(state);
        setError({
            your_name_err_msg: isEmpty(errors.yourNameErrMsg) ? "" : errors.yourNameErrMsg,
            your_email_err_msg: isEmpty(errors.replyEmailErrMsg) ? "" : errors.replyEmailErrMsg,
            your_message_err_msg: isEmpty(errors.messageErrMsg) ? "" : errors.messageErrMsg,
            your_phone_err_msg: isEmpty(errors.yourPhoneErrMsg) ? "" : errors.yourPhoneErrMsg,
        });
        return isValid;
    }

    const handleChange = (e) => {
        setState({ ...state, [e.target.name]: e.target.value });
    }



    if (state.isLoading) {
        return <Loader />
    }

    return (
        <>
            <div className='container-recent-history-div'>
                <div className='row'>
                    <RecentHistory />
                    <div className='col-md-9 p-0'>
                        <div className='capsule contact-support'>
                            <div className='title-color'>CONTACT SUPPORT</div>
                            <Form noValidate onSubmit={handleSubmit}>
                                <Form.Group className="mb-3 form-input-width">
                                    <Form.Label>Your Name</Form.Label>
                                    <Form.Control name="your_name" type="text" value={state.your_name} onChange={handleChange} />
                                    {!isEmpty(error.your_name_err_msg) && <span className='text-danger'>{error.your_name_err_msg}</span>}
                                </Form.Group>

                                <Form.Group className="mb-3 form-input-width">
                                    <Form.Label>Reply Email</Form.Label>
                                    <Form.Control name="your_email" type="email" value={state.your_email} onChange={handleChange} />
                                    {!isEmpty(error.your_email_err_msg) && <span className='text-danger'>{error.your_email_err_msg}</span>}
                                </Form.Group>
                                <Form.Group className="mb-3 form-input-width">
                                    <Form.Label>Reply Phone - (Optional)</Form.Label>
                                    <Form.Control name="your_phone" type="text" value={state.your_phone} onChange={handleChange} maxLength={10} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9]/g, '')} />
                                    {!isEmpty(error.your_phone_err_msg) && <span className='text-danger'>{error.your_phone_err_msg}</span>}
                                </Form.Group>

                                <Form.Group className="mb-3 form-textarea-width">
                                    <Form.Label>Message</Form.Label>
                                    <Form.Control as="textarea" name="your_message" rows="5" value={state.your_message} onChange={handleChange} />
                                    {!isEmpty(error.your_message_err_msg) && <span className='text-danger'>{error.your_message_err_msg}</span>}
                                </Form.Group>

                                <Button variant="default" type="submit">Send</Button>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Support;