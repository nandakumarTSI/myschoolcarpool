import React, { useState, useRef, useCallback, useEffect } from "react";
import { withRouter } from 'react-router-dom';
import { Button, Form, Row, Col } from 'react-bootstrap';
import Select from 'react-select';

import { isEmpty } from 'lodash';
import Loader from 'react-loader-spinner';
import { Error, Success } from "../ToastMessage/index";
import { validateInput } from '../Validation/SignUp';
import config from '../../config';

const useStateCallback = initialState => {
    const [state, setState] = useState(initialState);
    const cbRef = useRef(null); // init mutable ref container for callbacks

    const setStateCallback = useCallback((state, cb) => {
        cbRef.current = cb; // store current, passed callback in ref
        setState(state);
    }, []); // keep object reference stable, exactly like `useState`

    useEffect(() => {
        // cb.current is `null` on initial render, 
        // so we only invoke callback on state *updates*
        if (cbRef.current) {
            cbRef.current(state);
            cbRef.current = null; // reset callback after execution
        }
    }, [state]);

    return [state, setStateCallback];
};





const ManageAccount = (props) => {
    const [statelist, setStateList] = useState([]);
    const [state, setState] = useStateCallback({
        device_type: "android",
        device_token: "03df25c845d460bc",
        first_name: "",
        last_name: "",
        address_id: 0,
        address: "",
        city: "",
        _state: 0,
        _selectedState: { value: null, label: null },
        zip: 0,
        phone: "",
        email: "",
        alternate_address_id: 0,
        alternate_address: "",
        alternate_city: "",
        alternate_state: 0,
        _selectedAlternateState: { value: null, label: null },
        alternate_zip: 0,
        alternate_email: "",
        alternate_phone: "",
        isLoading: true
    });


    const [errorMsg, setErrorMsg] = useState({
        firtNameErrorMsg: "",
        lastNameErrorMsg: "",
        phoneErrorMsg: "",
        emailErrorMsg: "",
        addressErrorMsg: "",
        cityErrorMsg: "",
        stateErrorMsg: "",
        zipErrorMsg: "",
        passwordErrorMsg: "",
        confirmPasswordErrorMsg: "",
        alternateEmailErrorMsg: "",
        alternatePhoneErrorMsg: ""
    });



    useEffect(() => {
        fetch(`${config.apiURL}/getStates`, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then(response => {
                if (response.status && !isEmpty(response.body)) {
                    const getStateList = response.body.map((item) => ({
                        value: item.id,
                        label: item.name
                    }));
                    setStateList(getStateList);
                }
            });

        fetch(`${config.apiURL}/viewParent?id=${localStorage.getItem('user_id')}`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.status) {
                    const result = response.body;
                    const [address_info] = result.user_location_details.filter(x => x.is_default === 1);
                    const [alternate_address_info] = result.user_location_details.filter(x => x.is_default === 0);

                    setState({
                        ...state,
                        device_type: "android",
                        device_token: "03df25c845d460bc",
                        first_name: result.first_name,
                        last_name: result.last_name,
                        address_id: address_info.id,
                        address: address_info.address,
                        city: address_info.city,
                        _state: address_info.state,
                        _selectedState: { label: address_info.state_name, value: address_info.state },
                        zip: address_info.zip,
                        phone: address_info.phone,
                        email: result.email,
                        alternate_address_id: isEmpty(alternate_address_info) ? "" : alternate_address_info.id,
                        alternate_address: isEmpty(alternate_address_info) ? "" : alternate_address_info.address,
                        alternate_city: isEmpty(alternate_address_info) ? "" : alternate_address_info.city,
                        alternate_state: isEmpty(alternate_address_info) ? "" : alternate_address_info.state,
                        _selectedAlternateState: !isEmpty(alternate_address_info) && { label: alternate_address_info.state_name, value: alternate_address_info.state },
                        alternate_phone: isEmpty(alternate_address_info) ? "" : alternate_address_info.phone,
                        alternate_email: isEmpty(result.alternate_email) ? "" : result.alternate_email,
                        isLoading: false
                    });
                }
                else {
                    const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                    Error(msg);
                    setState({ ...state, isLoading: false });
                }
            })
            .catch(err => {
                Error(err.toString());
                setState({ ...state, isLoading: false });
            });
    }, []);

    const handleFormSubmit = (e) => {
        e.preventDefault();

        const { errors, isValid } = validateInput(state);
        setErrorMsg({
            firtNameErrorMsg: errors.firtNameErrorMsg,
            lastNameErrorMsg: errors.lastNameErrorMsg,
            phoneErrorMsg: errors.phoneErrorMsg,
            emailErrorMsg: errors.emailErrorMsg,
            addressErrorMsg: errors.addressErrorMsg,
            cityErrorMsg: errors.cityErrorMsg,
            stateErrorMsg: errors.stateErrorMsg,
            zipErrorMsg: errors.zipErrorMsg,
            passwordErrorMsg: errors.passwordErrorMsg,
            confirmPasswordErrorMsg: errors.confirmPasswordErrorMsg,
            alternateEmailErrorMsg: errors.alternateEmailErrorMsg,
            alternatePhoneErrorMsg: errors.alternatePhoneErrorMsg
        });

        if (isValid) {
            setState({ ...state, isLoading: true });

            const { device_type, device_token, first_name, last_name, phone, address, address_id, city, _state, zip, alternate_address, alternate_address_id, alternate_city, alternate_state, alternate_phone, password } = state;
            const url = config.apiURL + "/profile/update";

            fetch(url, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "device_type": device_type,
                    "device_token": device_token,
                    "first_name": first_name,
                    "last_name": last_name,
                    "address_id ": address_id,
                    "address": address,
                    "city": city,
                    "state": _state,
                    "zip": parseInt(zip),
                    "phone": phone,
                    "alternate_address_id": isEmpty(alternate_address_id) ? 0 : parseInt(alternate_address_id),
                    "alternate_address": alternate_address,
                    "alternate_city": alternate_city,
                    "alternate_state": alternate_state,
                    "alternate_phone": alternate_phone,
                    "password": password
                })
            })
                .then((response) => response.json())
                .then(response => {
                    if (response.status === true) {
                        const _CurrentUserName = response.body.first_name + " " + response.body.last_name;
                        localStorage.setItem("CurrentUserName", _CurrentUserName);
                        localStorage.setItem("token", response.body._token);
                        Success(response.message);
                        setState({ ...state, isLoading: false });
                    }
                    else {
                        const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                        Error(msg);
                        setState({ ...state, isLoading: false });
                    }
                })
                .catch(err => {
                    Error(err.toString());
                    setState({ ...state, isLoading: false });
                });
        }
    }

    const handleChange = (e) => {
        if (isEmpty(e.target)) {
            setState({ ...state, "_state": e.value, _selectedState: e });
        }
        else {
            setState({ ...state, [e.target.name]: e.target.value });
        }
    }

    const handleChangeAlternateState = (e) => {
        setState({ ...state, "alternate_state": e.value, _selectedAlternateState: e });
    }

    const isLoading = state.isLoading;

    return (
        <div className='school-admin-portal'>
            <div className='admin-portal-container px-md-5 sign-up-capsule'>
                <div className='admin-portal-title text-center'>Manage Account</div>
                {
                    isLoading ?
                        <Loader type="Triangle" color="#6d4c96" height={100} width={100} className="text-center" />
                        :
                        <Form noValidate onSubmit={handleFormSubmit}>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="6">
                                    <Form.Control name="first_name" className="mb-xs-3" type="text" placeholder="First" onChange={handleChange} value={state.first_name} value={state.first_name} />
                                    <Form.Text className="text-danger">{errorMsg.firtNameErrorMsg}</Form.Text>
                                </Col>
                                <Col sm="6">
                                    <Form.Control name="last_name" type="text" placeholder="Last" onChange={handleChange} value={state.last_name} />
                                    <Form.Text className="text-danger">{errorMsg.lastNameErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="phone" type="text" placeholder="Phone" maxLength={10} onChange={handleChange} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9 ]/g, '')} value={state.phone} />
                                    <Form.Text className="text-danger">{errorMsg.phoneErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="email" type="email" placeholder="Email Address" onChange={handleChange} value={state.email} readOnly />
                                    <Form.Text className="text-danger">{errorMsg.emailErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="address" type="text" placeholder="Address" onChange={handleChange} value={state.address} />
                                    <Form.Text className="text-danger">{errorMsg.addressErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pb-5">
                                <Col sm="6">
                                    <Form.Control name="city" className="mb-xs-3" type="text" placeholder="City" onChange={handleChange} value={state.city} />
                                    <Form.Text className="text-danger">{errorMsg.cityErrorMsg}</Form.Text>
                                </Col>
                                <Col sm="3">
                                    <Select
                                        name="_state"
                                        className="mb-xs-3 custom-dropdown"
                                        placeholder="State"
                                        defaultValue={state._selectedState}
                                        options={statelist}
                                        value={state._selectedState}
                                        isSearchable={true}
                                        onChange={handleChange}
                                    />
                                    <Form.Text className="text-danger">{errorMsg.stateErrorMsg}</Form.Text>
                                </Col>
                                <Col sm="3">
                                    <Form.Control name="zip" type="text" placeholder="Zip" maxLength={5} onChange={handleChange} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9]/g, '')} value={state.zip} />
                                    <Form.Text className="text-danger">{errorMsg.zipErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pt-3 border-top border-1 border-dark">
                                <Col sm="12">
                                    <Form.Label>Some additional information</Form.Label>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="alternate_address" type="text" placeholder="Alternate Address (optional)" onChange={handleChange} value={state.alternate_address} />
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="8">
                                    <Form.Control name="alternate_city" className="mb-xs-3" type="text" placeholder="City (optional)" onChange={handleChange} value={state.alternate_city} />
                                </Col>
                                <Col sm="4">
                                    <Select
                                        name="alternate_state"
                                        className="custom-dropdown"
                                        placeholder="State (optional)"
                                        defaultValue={state._selectedAlternateState}
                                        options={statelist}
                                        value={state._selectedAlternateState}
                                        isSearchable={true}
                                        onChange={handleChangeAlternateState}
                                    />
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="alternate_phone" type="text" placeholder="Alternate Phone (optional)" maxLength={10} onChange={handleChange} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9]/g, '')} value={state.alternate_phone} />
                                    <Form.Text className="text-danger">{errorMsg.alternatePhoneErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pb-5">
                                <Col sm="12">
                                    <Form.Control name="alternate_email" type="email" placeholder="Alternate Email Address (optional)" onChange={handleChange} value={state.alternate_email} readOnly />
                                    <Form.Text className="text-danger">{errorMsg.alternateEmailErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pt-3 border-top border-1 border-dark">
                                <Col sm="12">
                                    <Form.Label>Choose a password</Form.Label>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="password" type="password" placeholder="Password..." minLength={6} maxLength={20} onChange={handleChange} value={state.password} />
                                    <Form.Text className="text-danger">{errorMsg.passwordErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="confirm_password" type="password" placeholder="Confirm Password..." minLength={6} maxLength={20} onChange={handleChange} value={state.confirm_password} />
                                    <Form.Text className="text-danger">{errorMsg.confirmPasswordErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Form.Text className="text-muted pb-3">
                                    You can manage other account settings within our mobile app.
                                </Form.Text>
                                <Col sm="12">
                                    <Button variant="primary col-sm-12" type="submit" >
                                        Update Account
                                    </Button>
                                </Col>
                            </Form.Group>
                        </Form>
                }
            </div>
        </div>
    );

}

export default withRouter(ManageAccount);