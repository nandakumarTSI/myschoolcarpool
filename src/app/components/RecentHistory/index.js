import React, { Component } from 'react';
//import './style.css';
import moment from 'moment';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { createGetRecentHistoryRequest } from '../../actions/GetRecentHistory';
import Loader from 'react-loader-spinner';

class RecentHistory extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            errorMsg: ''
        };
    }

    componentDidMount = () => {
        this.props.createGetRecentHistoryRequest()
            .then(
                res => {

                },
                err => {
                    this.setState({ errorMsg: err.toString() });
                }
            )
            .then(x => {
                this.setState({ isLoading: false });
            });
    }

    render() {
        if (localStorage.getItem("token") === null || localStorage.getItem("token") === "") {
            return <Redirect to="/login" />
        }
        else {
            const isLoading = this.state.isLoading;
            const { recentHistory } = this.props;

            let errorMsg = "";
            if (isEmpty(recentHistory)) {
                errorMsg = "No Recent History";
            }
            else if (!recentHistory.status) {
                errorMsg = (!isEmpty(recentHistory.error_code) ? "Error Code: " + recentHistory.error_code : "") + " " + (!isEmpty(recentHistory.message) ? "Message: " + recentHistory.message : "");
            }
            else if (isEmpty(recentHistory.body)) {
                errorMsg = "No Recent History";
            }
            else {
                errorMsg = "";
            }

            return (
                <div className='recent-history-container col-md-3 mb-5 mb-md-0 p-0'>
                    <div className='recent-history-title'>Recent History</div>
                    {
                        isLoading ?
                            <Loader type="Triangle" color="#6d4c96" height={100} width={100} className="text-center" />
                            :
                            <div className='recent-history-info'>
                                {isEmpty(errorMsg)
                                    ?
                                    recentHistory.body.map((response) => {
                                        let recentHistoryStatus = "";

                                        if (response.moduletype === 3 || response.moduletype === 4) {
                                            return <div key={response.id}></div>;
                                        }

                                        if (response.moduletype === 1 || response.moduletype === 2) {
                                            recentHistoryStatus = "Pending Approval: ";
                                        }
                                        else if (response.moduletype === 5) {
                                            recentHistoryStatus = "Group Ride Schedule Member Status Update To Admin: ";
                                        }
                                        else if (response.moduletype === 6) {
                                            recentHistoryStatus = "Group Member Update Status: ";
                                        }
                                        else if (response.moduletype === 7) {
                                            recentHistoryStatus = "Group Member Leave: ";
                                        }
                                        else if (response.moduletype === 8) {
                                            recentHistoryStatus = "Group Admin Update Member Status: ";
                                        }

                                        let recentHistoryDateTime = isEmpty(response.updated_at) ? response.created_at : response.updated_at;

                                        return <div key={response.id} className='recent-history-status-info'>
                                            <p className='rh-status-plus-user-name'>{recentHistoryStatus} {response.sender_name.first_name} {response.sender_name.last_name}</p>
                                            <p className='rh-status-datetime'>{moment(new Date(recentHistoryDateTime)).format("DD/MM/YYYY | hh:mm a")} MST</p>
                                        </div>
                                    })
                                    :
                                    <div className='warn-msg'>{!isEmpty(this.state.errorMsg) ? this.state.errorMsg : errorMsg}</div>
                                }
                            </div>
                    }
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    const { getRecentHistory } = state
    return {
        recentHistory: getRecentHistory.data
    }
}

export default connect(mapStateToProps, { createGetRecentHistoryRequest })(RecentHistory);