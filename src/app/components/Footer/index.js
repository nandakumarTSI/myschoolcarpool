import React from 'react';
//import './style.css';

const footer = () =>
  <footer>
    <div>
      &#169; 2021 My School Carpool LLC
    </div>
  </footer>;

export default footer;
