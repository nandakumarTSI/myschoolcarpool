import React, { Component } from 'react';
//import './style.css';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';

import { createResetPasswordRequest } from '../../actions/ResetPasswordAction';
import { validateInput } from '../Validation/ForgotPassword';
import Loader from '../Loader/index';

class ForgotPassword extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            errorMsg: '',
            isLoading: false
        };

        this.onChange = this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { errors, isValid } = validateInput(this.state);
        
        if (!isValid) {
            this.setState({
                errorMsg: errors.emailErrorMsg
            });
        }
        else {
            this.setState({
                isLoading: true
            });
            this.props.createResetPasswordRequest(this.state)
                .then(
                    result => {
                        if (result.user.status === true) {
                            this.setState({
                                errorMsg: ''
                            });
                            this.props.history.push('/forgotpasswordresponse');
                        }
                        else {
                            this.setState({
                                errorMsg: result.user.message
                            });
                        }
                    },
                    err => {
                        this.setState({
                            errorMsg: err.toString()
                        });
                    }
                )
                .then(x => {
                    this.setState({
                        email: '',
                        isLoading: false
                    });
                });
        }


    }

    render() {
        const isLoading = this.state.isLoading;
        if (isLoading) {
            return <Loader />
        }
        return (
            <div className='forgot-password-container'>
                <form onSubmit={this.handleSubmit}>
                    <div className='forgot-password-title'>Reset your password</div>
                    <input type='email' name='email' className='forgot-password-email-field' placeholder='Enter your email' onChange={this.onChange} />
                    <span className='warn-msg'>{this.state.errorMsg}</span>
                    <button>Send Reset Instructions</button>
                </form>
            </div>
        );
    }
}

export default withRouter(connect(null, { createResetPasswordRequest })(ForgotPassword));