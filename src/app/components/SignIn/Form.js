import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

//import './style.css';
import { createLoginRequest } from '../../actions/LoginAction';
import { validateInput } from '../Validation/SignIn';


class SigninForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            device_type: 'android',
            device_token: 'hkfleqerqwer',
            emailErrorMsg: '',
            passwordErrorMsg: ''
        };

        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleLogin = () => {
        this.setState({
            emailErrorMsg: "",
            passwordErrorMsg: ""
        });
        const { errors, isValid } = validateInput(this.state);

        if (!isValid) {
            this.setState({
                emailErrorMsg: errors.emailErrorMsg,
                passwordErrorMsg: errors.passwordErrorMsg
            });
        }
        else {
            this.props.createLoginRequest(this.state)
                .then(
                    result => {
                        if (result.user.status === true) {
                            localStorage.setItem('token', result.user.body._token);
                            localStorage.setItem('user_id', result.user.body.user_id);
                            localStorage.setItem('CurrentUserName', result.user.body.first_name + " " + result.user.body.last_name);
                            document.body.classList.remove('login-page');
                            this.props.history.push('/dashboard');
                        }
                        else {
                            this.setState({
                                emailErrorMsg: "",
                                passwordErrorMsg: result.user.message
                            });
                        }
                    },
                    err => {
                        this.setState({
                            emailErrorMsg: "",
                            passwordErrorMsg: err.toString()
                        });
                    }
                );
        }
    }

    handleSignUp = () => {
        this.props.history.push("/signup");
    }

    render() {
        return (
            <div className='school-admin-portal' style={{ maxWidth: '1200px' }}>
                <div className='admin-portal-container'>
                    <div className='admin-portal-title'>Group Administration Portal</div>
                    <div className='admin-portal-sub-title'>Manage the parents and drivers connected with your My School Carpool school profile.</div>
                    <div className='admin-portal-input-ctrls'>
                        <input type='email' name='email' className='email-field' onChange={this.onChange} placeholder='Email' />
                        <span className='warn-msg'>{this.state.emailErrorMsg}</span>
                        <input type='password' name='password' className='password-field' onChange={this.onChange} placeholder='Password' />
                        <span className='warn-msg'>{this.state.passwordErrorMsg}</span>
                        <div className='sign-btns'>
                            <button onClick={() => this.handleLogin()}>Log in</button>
                            <button onClick={() => this.handleSignUp()}>Sign up</button>
                        </div>
                    </div>
                    <Link to='/forgotpassword'>Forgot password?</Link>
                </div>
            </div>
        );
    }
}

export default withRouter(connect(null, { createLoginRequest })(SigninForm));
