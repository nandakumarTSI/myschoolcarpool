import React from 'react';
import Loader from 'react-loader-spinner';

export default function Spinner() {
    return (
        <div className='loadingContainer'>
            <Loader
                type="Triangle"
                color="#6d4c96"
                height={200}
                width={200}
            />
        </div>
    );
}