import React from "react";

const TermsOfService = () => {
    document.body.classList.remove('login-page');

    return (
        <div className='school-admin-portal terms-service-capsule'>
            <div className='admin-portal-container px-md-5'>
                <h1 className="text-center terms-service-title">Terms of Service</h1>
                <p>WELCOME TO MY SCHOOL CARPOOL</p>
                <p>The Service</p>
                <p>
                    My School Carpool is a School Bus Replacement Platform & School Carpooling App licensed by Drivers and/or provided through the My School Carpool Web Portal & My School Carpool App (My School Carpool), to connect My School Carpool parents and their student users that are looking for a ride (“Riders”) with My School Carpool parents or legal guardian drivers that may be headed in the same direction (“Drivers”).
                </p>
                <p>
                    My School Carpool is provided by My School Carpool, LLC. with its mailing address at P.O. Box 31279, Santa Fe, NM 87594 according to the below terms.. By using My School Carpool, you are agreeing to these terms, including the other terms and policies referenced here (the “My School Carpool Terms”).
                </p>
                <p>Important Note</p>
                <p>
                    MY SCHOOL CARPOOL IS A PLATFORM SERVICE THAT CONNECTS STUDENT RIDERS AND THEIR PARENTS WITH PARENT DRIVERS. ALL CARPOOL ARRANGEMENTS MADE THROUGH MY SCHOOL CARPOOL ARE BETWEEN STUDENT RIDER’S PARENTS AND PARENT DRIVERS. MY SCHOOL CARPOOL WILL NOT BE RESPONSIBLE FOR:
                </p>
                <p>
                    1 THE ACTIONS, ERRORS OR OMISSIONS OF ANY MY SCHOOL CARPOOL USER;
                </p>
                <p>
                    2 THE TRUTH OR ACCURACY OF ANY INFORMATION PROVIDED BY A MY SCHOOL CARPOOL USER; OR
                </p>
                <p>
                    3 A MY SCHOOL CARPOOL USER’S COMPLIANCE WITH THE TERMS OR ANY APPLICABLE LAW. MY SCHOOL CARPOOL DISCLAIMS ALL LIABILITY FOR THE ACTIONS, ERRORS OR OMISSIONS OF MY SCHOOL CARPOOL USERS.
                </p>
                <p>
                    USING MY SCHOOL CARPOOL
                </p>
                <p>
                    General Information.
                </p>
                <p>
                    You may only use My School Carpool to arrange a carpool ride for the physical transport of a Student Rider by a Parent Driver (a “Ride”).You may not use My School Carpool to provide or procure any other services (such as carpooling with someone other than students or package deliveries.).
                </p>
                <p>
                    Misuse of My School Carpool
                </p>
                <p>
                    You may use My School Carpool only as permitted by law, including applicable export and re-export control laws and regulations.Don’t misuse My School Carpool.For example:
                </p>
                <p>
                    1 Don’t interfere with My School Carpool or try to access it using a method other than the interface and the instructions that we provide; and
                </p>
                <p>
                    2 Respect the privacy of your fellow users.In particular, do not misuse personal information about Riders or Drivers that you may access through your use of the Service.
                </p>
                <p>
                    We may suspend or stop providing My School Carpool to you if you do not comply with our terms or policies or if we are investigating suspected misconduct.
                </p>
                <p>
                    Intellectual Property
                </p>
                <p>
                    Using My School Carpool does not give you ownership of any intellectual property rights in My School Carpool or the content you access.You may not use content from My School Carpool unless you obtain permission from its owner or are otherwise permitted by law.These terms do not grant you the right to use any branding or logos used in My School Carpool.Don’t remove, obscure, or alter any legal notices displayed in or along with My School Carpool.
                </p>
                <p>
                    Third Party Content
                </p>
                <p>
                    My School Carpool displays some content that is not ours.This content is the sole responsibility of the entity that makes it available.We may review content to determine whether it is illegal or violates our policies, and we may remove or refuse to display content that we reasonably believe violates our policies or the law.But that does not necessarily mean that we review content, so please don’t assume that we do.
                </p>
                <p>
                    Communications
                </p>
                <p>
                    In connection with your use of My School Carpool, we may send you notifications, service announcements, administrative messages, promotions and other information, via email, SMS, and other channels.You may opt out of any or all of those communications (other than important communications relating to your account or confirmations) at any time.
                </p>
                <p>
                    Safe Use of the Service
                </p>
                <p>
                    My School Carpool is available on mobile devices.Do not use My School Carpool in a way that distracts you or prevents you from obeying traffic or safety laws.
                </p>
                <p>
                    Arranging a Ride
                </p>
                <p>
                    Please be considerate when using My School Carpool.When you schedule a Ride with a My School Carpool Driver, you are committing to meet that Driver at a specified location at a certain time and agreeing to share a Ride with that user, so be sure you want to share the Ride before requesting or accepting it, and make best efforts to be at the meeting point on time.
                </p>
                <p>
                    Cancelling a Ride
                </p>
                <p>
                    You can cancel a Ride, but it may affect your ability to request or accept future Rides.
                </p>

                <p>
                    DRIVER-SPECIFIC TERMS
                </p>
                <p>
                    Driver Eligibility Criteria
                </p>
                <p>
                    If you are a Parent Driver and want to provide Rides through My School Carpool, you must:
                </p>
                <p>
                    1 Hold a valid driving license for the area in which you are driving;
                </p>
                <p>
                    2 Be a parent or legal guardian of a student Rider;
                </p>
                <p>
                    3 Pass a background check;
                </p>
                <p>
                    4 Drive safely and in accordance with all applicable driving rules and laws;
                </p>
                <p>
                    5 Ensure your vehicle is roadworthy, and has all certifications required by law;
                </p>
                <p>
                    6 Have appropriate insurance coverage for all rides and any accidents that may occur during a ride, including without limitation personal injury protection insurance coverage for your vehicle with no less than the minimum limits required by applicable law;
                </p>
                <p>
                    7 Only provide rides in a personal capacity and not in the course of a business;
                </p>
                <p>
                    8 Not use My School Carpool to generate a profit; and
                </p>
                <p>
                    9 Not use My School Carpool to provide rides on motorcycles.
                </p>
                <p>
                    Providing Rides
                </p>
                <p>
                    You agree to not misrepresent in any way, whether material or not, any aspect of your identity, background, credentials, regulated status or professional standing when using My School Carpool. You agree to comply with all applicable laws and regulations in the jurisdictions in which you provide or offer to provide Rides. If your offer or provision of a Ride requires a license or other regulatory approval, you must first obtain that license or approval and remain in good standing before offering or providing the Ride.
                </p>
                <p>
                    Information Shared With Riders
                </p>
                <p>
                    The following information may be visible to other users in My School Carpool to help connect you with Riders:
                </p>
                <p>
                    1 Your name and My School Carpool profile picture;
                </p>
                <p>
                    2 Your vehicle make, model, color and registration identifier (license plate);
                </p>
                <p>
                    3 How long you have been a My School Carpool user;
                </p>
                <p>
                    4 The number of rides you have completed as a Rider and as a Driver; and
                </p>
                <p>
                    5 The number of times you have been thanked in connection with a ride by other My School Carpool users.
                </p>
                <p>
                    6 Other profile information that you have added or that is shown on your profile screen
                </p>
                <p>
                    In addition, while you are in route to pick up a Rider and for the duration of each Ride, My School Carpool will share your current location and your estimated time of arrival at the agreed pickup location with the applicable Rider.
                </p>
                <p>
                    RIDER-SPECIFIC TERMS
                </p>
                <p>
                    Verifying Driver Details
                </p>
                <p>
                    Although My School Carpool may in some instances attempt to verify certain information provided by Drivers, My School Carpool has no obligation to do so and makes no representations regarding the accuracy of such verification.It is your administrator’s responsibility to verify the identity, qualifications and status of a Driver.
                </p>
                <p>
                    Information Shared With Drivers and Riders
                </p>
                <p>
                    The following information may be visible to other users in My School Carpool to help connect you with Drivers and other Riders:
                </p>
                <p>
                    1 The ride you have requested;
                </p>
                <p>
                    2 Your name and My School Carpool profile picture;
                </p>
                <p>
                    3 How long you have been a My School Carpool application user;
                </p>
                <p>
                    4 How long you have been a My School Carpool user;
                </p>
                <p>
                    5 The number of Rides you have completed as a Rider and as a Driver; and
                </p>
                <p>
                    6 The number of times you have been thanked in connection with a Ride by other My School Carpool users.
                </p>
                <p>
                    7 Other profile information that you have added
                </p>
                <p>
                    PRICING AND PAYMENTS
                </p>
                <p>
                    Using the Carpooling System
                </p>
                <p>
                    My School Carpool is a carpooling system whereby parents of student riders share the costs on a per mile basis to cover the Driver’s costs of gas and vehicle depreciation in connection with such Ride (the “Price”), and the Driver and Rider must mutually agree on the Price per mile before the Ride is confirmed.
                </p>
                <p>
                    Driver may pick up multiple Riders and each may make contributions towards the Driver’s costs.The Price between each Rider and the Driver may vary depending on total miles traveled.
                </p>
                <p>
                    Any payments (including payment requests and transfers) must be made exclusively through the My School Carpool platform.
                </p>
                <p>
                    My School Carpool’s Subscription Cost
                </p>
                <p>
                    Parents agree that My School Carpool may charge the parents of each student rider a subscription fee of $2.99 per month.
                </p>
                <p>
                    Completed Rides
                </p>


                <p>
                    Parent’s Riders account will be charged upon each completed ride, accounted for on a weekly basis, and charged to a credit card weekly.are obligated to pay the Price to Drivers after the Ride is completed.A Ride is completed after the Driver drops the Rider off at the drop-off point.
                </p>

                <p>
                    Payment Terms

                </p>
                <p>
                    Upon signing up for My School Carpool, an initial deposit into your account is required.When you are the Parent of a student Rider, your account will be debited on a daily basis and reconciled weekly.When you are a Parent Driver, your account will be credited on a daily basis and reconciled weekly.

                </p>
                <p>
                    Compliance with Tax Laws

                </p>
                <p>
                    You must comply with any and all applicable tax laws, including the reporting and payment of any taxes arising in connection with your use of My School Carpool or payments made through My School Carpool.The reporting and payment of any such applicable taxes are your responsibility.

                </p>
                <p>
                    Taxes

                </p>
                <p>
                    Drivers are responsible for determining if the Reimbursement is taxable, and for remitting taxes to the appropriate tax authority.Riders must pay for Rides without any reduction for Taxes.”Taxes” means any duties, customs fees, or taxes (other than income tax) associated with providing Rides, including any related penalties or interest.

                </p>
                <p>
                    Reimbursements and Chargebacks

                </p>
                <p>
                    My School Carpool is not responsible for any fraudulent use of My School Carpool or for any chargebacks, reversals or other failure of a Rider to pay the Price.Where a Rider charges back or otherwise reverses a transaction, Drivers agree to return any corresponding amount already provided by My School Carpool.My School Carpool may set off any amounts owed to Drivers against any amounts that Driver owes (for example, as a result of chargebacks or reversals).

                </p>
                <p>
                    DISPUTES

                </p>
                <p>
                    Any disputes relating to Rides and carpooling agreements are between Riders and Drivers.My School Carpool is under no obligation to resolve disputes between My School Carpool users.If a Rider and Driver resolve a dispute in the Rider’s favor, My School Carpool may deduct the value of any overpaid Reimbursement from future Reimbursements due to the Driver.

                </p>
                <p>
                    PRIVACY

                </p>
                <p>
                    Account Information

                </p>
                <p>
                    In order to use My School Carpool, you may be required to provide information about yourself, including your name, phone number, and email address.If you are a Driver, you will also be required to provide your vehicle’s details, and you agree that My School Carpool may import all of this information from your My School Carpool profile.You agree that any such information that you provide in connection with My School Carpool will always be accurate, correct and up to date.

                </p>
                <p>
                    In addition, in order to make or receive payments through My School Carpool, you may be required to provide My School Carpool with payment details if you are a Rider (such as credit card number and expiration date) and bank account information if you are a Driver.To protect your privacy, your payment and bank account details may not be stored on My School Carpool’s systems but rather on a third party online payment gateway that assists My School Carpool in the authentication, authorization, charge and maintenance of your payment and bank account details.This is with the provision that such gateway is subject to applicable standards and regulations with respect to the holding of personal information and the processing of online payments.

                </p>
                <p>
                    My School Carpool Account

                </p>
                <p>
                    You need a My School Carpool Account in order to use My School Carpool, and My School Carpool uses information from your My School Carpool Account.You agree that your My School Carpool Account information may be provided to My School Carpool and used in connection with My School Carpool, and that information provided to My School Carpool.will be controlled and processed by My School Carpool according to these My School Carpool Terms.In addition, My School Carpool may add My School Carpool events for scheduled Rides to your My School Carpool Calendar.

                </p>
                <p>
                    Location Data

                </p>
                <p>
                    Some features of My School Carpool make use of detailed location and route information, for example in the form of GPS signals and other information sent by the mobile device on which you use My School Carpool.These features cannot be provided without this technology.

                </p>
                <p>
                    COPYRIGHT PROTECTION

                </p>
                <p>
                    We respond to notices of alleged copyright infringement and terminate accounts of repeat infringers according to the process set out in the U.S.Digital Millennium Copyright Act.We provide information to help copyright holders manage their intellectual property online.If you think somebody is violating your copyrights in connection with My School Carpool, you can submit a notice to My School Carpool.

                </p>
                <p>
                    YOUR CONTENT IN MY SCHOOL CARPOOL

                </p>
                <p>
                    My School Carpool may allow you to upload, submit, store, send or receive content.You retain ownership of any intellectual property rights that you hold in that content.In short, what belongs to you stays yours.

                </p>
                <p>
                    When you upload, submit, store, send or receive content to or through My School Carpool, you give My School Carpool and its affiliates (and those we work with) a worldwide license to use, host, store, reproduce, modify, create derivative works (such as those resulting from translations, adaptations or other changes we make so that your content works better with My School Carpool), communicate, publish, publicly perform, publicly display and distribute such content.The rights you grant in this license are for the limited purpose of operating, promoting, and improving My School Carpool, and to develop new services.This license continues even if you stop using My School Carpool.Make sure you have the necessary rights to grant us this license for any content that you submit to My School Carpool.

                </p>
                <p>
                    ABOUT SOFTWARE IN MY SCHOOL CARPOOL
                </p>
                <p>
                    My School Carpool’s mobile application may update automatically on your device once a new version or feature is available.
                </p>
                <p>
                    My School Carpool gives you a personal, worldwide, royalty-free, non-assignable and non-exclusive license to use the software provided to you by us as part of My School Carpool.This license is for the sole purpose of enabling you to use and enjoy the benefit of My School Carpool as provided by us, in the manner permitted by these terms.You may not copy, modify, distribute, sell, or lease any part of My School Carpool or included software, nor may you reverse engineer or attempt to extract the source code of that software, unless laws prohibit those restrictions, or you have our written permission.
                </p>
                <p>
                    Open source software is important to us.Some software used in My School Carpool may be offered under an open source license that we will make available to you.There may be provisions in the open source license that expressly override some of these terms.
                </p>
                <p>
                    OUR WARRANTIES AND DISCLAIMERS
                </p>
                <p>
                    We provide My School Carpool using a commercially reasonable level of skill and care and we hope that you will enjoy using it.But there are certain things that we do not promise about My School Carpool.Other than as expressly set out in these terms, My School Carpool does not make any specific promises about My School Carpool.For example, we don’t make any commitments about the content within My School Carpool, the specific functions of My School Carpool, or its reliability, availability, or ability to meet your needs.My School Carpool is provided “AS IS”.
                </p>
                <p>
                    Some jurisdictions provide for certain warranties, like the implied warranty of merchantability, fitness for a particular purpose and non-infringement.To the extent permitted by law, we exclude all warranties.
                </p>
                <p>
                    All carpooling agreements made through My School Carpool are between the Parents of student Riders and Drivers.By providing the My School Carpool platform, My School Carpool acts solely as an intermediary and does not enter any carpooling agreements itself.My School Carpool does not vet My School Carpool users.
                </p>
                <p>
                    LIABILITY FOR MY SCHOOL CARPOOL
                </p>
                <p>
                    When permitted by law, My School Carpool and My School Carpool’s suppliers and distributors will not be responsible for lost profits, revenues or data, financial losses or indirect, special, consequential, exemplary or punitive damages.
                </p>
                <p>
                    My School Carpool will have no liability for:
                </p>
                <p>
                    1 The actions, errors or omissions of any My School Carpool user;
                </p>
                <p>
                    2 The truth or accuracy of any information provided by any My School Carpool user;
                </p>
                <p>
                    3 The compliance by any My School Carpool user with these terms or applicable laws.
                </p>
                <p>
                    To the extent permitted by law, the total liability of My School Carpool and its suppliers and distributors for any claims under these terms, including for any implied warranties, is limited to $100 USD.
                </p>
                <p>
                    In all cases, My School Carpool and its suppliers and distributors will not be liable for any loss or damage that is not reasonably foreseeable.
                </p>
                <p>
                    We recognize that in some countries, you might have legal rights as a consumer.If you are using My School Carpool for a personal purpose, then nothing in these terms or any additional terms limits any consumers’ legal rights which may not be waived by contract.
                </p>
                <p>
                    ABOUT THESE MY SCHOOL CARPOOL TERMS
                </p>
                <p>
                    We may modify any terms that apply to My School Carpool to, for example, reflect changes to the law or changes to our services.You should look at the terms regularly.We’ll post notice of modifications to My School Carpool Terms on this page.We’ll post notice of modified additional terms in the applicable service.Changes will not apply retroactively and will become effective no sooner than fourteen days after they are posted.However, changes addressing new functions for a service or changes made for legal reasons will be effective immediately.If you do not agree to the modified terms for a service, you should discontinue your use of My School Carpool.
                </p>
                <p>
                    If there is a conflict between these My School Carpool Terms and any additional terms, the additional terms will control for that conflict.
                </p>
                <p>
                    These terms control the relationship between My School Carpool and you.They do not create any third party beneficiary rights.
                </p>
                <p>
                    Neither these terms nor your use of My School Carpool will be construed as creating any relationship, partnership, joint venture, employer-employee, agency, or franchisor-franchisee relationship in any way and of any kind between you and My School Carpool.
                </p>
                <p>
                    We are constantly changing and improving My School Carpool.We may add or remove functionalities or features, and we may suspend or stop My School Carpool altogether.
                </p>
                <p>
                    If you do not comply with these terms, and we don’t take action right away, this doesn’t mean that we are giving up any rights that we may have (such as taking action in the future).
                </p>
                <p>
                    If it turns out that a particular term is not enforceable, this will not affect any other terms.
                </p>
                <p>
                    The laws of New Mexico, U.S.A., excluding New Mexico’s conflict of laws rules, will apply to any disputes arising out of or relating to these terms or the Services, all claims arising out of or relating to these terms or the Services will be litigated exclusively in the federal or state courts of Santa Fe County, New Mexico, USA, and you and My School Carpool consent to personal jurisdiction in those courts.
                </p>
                <p>
                    Notwithstanding the prior sentence, we recognize that the courts in some countries will not apply New Mexico law to certain disputes.If you reside in one of those countries, then where New Mexico law is excluded from applying, your country’s laws will apply to such disputes (contractual or non-contractual) arising out of or relating to these terms or the Service.Similarly, if the courts in your country will not permit you to consent to the jurisdiction and venue of the courts in Santa Fe County, then your local jurisdiction and venue will apply to such disputes.
                </p>
                <p>
                    Either party may apply to any court for an injunction or other relief to protect its intellectual property rights.
                </p>
                <p>
                    For information about how to contact My School Carpool, please visit our contact page.
                </p>
                <button onClick={() => window.close()}>BACK</button>
            </div>
        </div >
    );
}

export default TermsOfService;