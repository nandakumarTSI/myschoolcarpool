import React, { useState, useRef, useCallback, useEffect } from "react";
import { Link, withRouter } from 'react-router-dom';
import { Button, Form, Row, Col } from 'react-bootstrap';
import Select from 'react-select';

import { isEmpty } from 'lodash';
import Loader from 'react-loader-spinner';
import { Error, Success } from "../ToastMessage/index";
import { validateInput } from '../Validation/SignUp';
import config from '../../config';

const useStateCallback = initialState => {
    const [state, setState] = useState(initialState);
    const cbRef = useRef(null); // init mutable ref container for callbacks

    const setStateCallback = useCallback((state, cb) => {
        cbRef.current = cb; // store current, passed callback in ref
        setState(state);
    }, []); // keep object reference stable, exactly like `useState`

    useEffect(() => {
        // cb.current is `null` on initial render, 
        // so we only invoke callback on state *updates*
        if (cbRef.current) {
            cbRef.current(state);
            cbRef.current = null; // reset callback after execution
        }
    }, [state]);

    return [state, setStateCallback];
};



const SignUp = (props) => {
    const [state, setState] = useStateCallback({
        device_type: "android",
        device_token: "03df25c845d460bc",
        driver: "",
        addresses: "",
        student: "",
        first_name: "",
        last_name: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        _state: "",
        statelist: [],
        zip: "",
        is_driver: false,
        password: "",
        confirm_password: "",
        is_terms_service_agreed: false,
        alternate_address: "",
        alternate_city: "",
        alternate_email: "",
        alternate_phone: "",
        alternate_state: "",
        isLoading: false
    });


    const [errorMsg, setErrorMsg] = useState({
        firtNameErrorMsg: "",
        lastNameErrorMsg: "",
        phoneErrorMsg: "",
        emailErrorMsg: "",
        addressErrorMsg: "",
        cityErrorMsg: "",
        stateErrorMsg: "",
        zipErrorMsg: "",
        passwordErrorMsg: "",
        confirmPasswordErrorMsg: "",
        alternateEmailErrorMsg: "",
        alternatePhoneErrorMsg: ""
    });


    useEffect(() => {
        document.body.classList.remove('login-page');

        fetch(`${config.apiURL}/getStates`, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then(response => {
                if (response.status && !isEmpty(response.body)) {
                    const getStateList = response.body.map((item) => ({
                        value: item.id,
                        label: item.name
                    }));
                    setState({ ...state, statelist: getStateList });
                } else {
                    setState({ ...state, statelist: [] });
                }
            });
    }, []);

    const handleFormSubmit = (e) => {
        e.preventDefault();

        const { errors, isValid } = validateInput(state);
        setErrorMsg({
            firtNameErrorMsg: errors.firtNameErrorMsg,
            lastNameErrorMsg: errors.lastNameErrorMsg,
            phoneErrorMsg: errors.phoneErrorMsg,
            emailErrorMsg: errors.emailErrorMsg,
            addressErrorMsg: errors.addressErrorMsg,
            cityErrorMsg: errors.cityErrorMsg,
            stateErrorMsg: errors.stateErrorMsg,
            zipErrorMsg: errors.zipErrorMsg,
            passwordErrorMsg: errors.passwordErrorMsg,
            confirmPasswordErrorMsg: errors.confirmPasswordErrorMsg,
            alternateEmailErrorMsg: errors.alternateEmailErrorMsg,
            alternatePhoneErrorMsg: errors.alternatePhoneErrorMsg
        });

        if (isValid) {
            setState({ isLoading: true });

            const { device_type, device_token, first_name, last_name, phone, email, address, city, _state, zip, is_driver, alternate_address, alternate_city, alternate_state, alternate_phone, alternate_email, password } = state;
            const url = config.apiURL + "/register";

            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "device_type": device_type,
                    "device_token": device_token,
                    "first_name": first_name,
                    "last_name": last_name,
                    "phone": phone,
                    "email": email,
                    "address": address,
                    "city": city,
                    "state": parseInt(_state),
                    "zip": parseInt(zip),
                    "is_driver": is_driver,
                    "alternate_address": alternate_address,
                    "alternate_city": alternate_city,
                    "alternate_state": alternate_state,
                    "alternate_phone": alternate_phone,
                    "alternate_email": alternate_email,
                    "password": password,
                    "is_web": true
                })
            })
                .then((response) => response.json())
                .then(response => {
                    if (response.status === true) {
                        if (!isEmpty(response.message)) {
                            fetch(`${config.apiURL}/updateNewUserStatus`, {
                                method: 'PUT',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({
                                    "email": email,
                                    'status': "1"
                                })
                            })
                                .then((response) => response.json())
                                .then(response => {
                                    if (response.status === true) {
                                        Success("Registered Successfully");
                                        props.history.push("login");
                                    }
                                    else {
                                        const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                                        Error(msg);
                                        setState({ ...state, isLoading: false });
                                    }
                                })
                                .catch(err => {
                                    Error(err.toString());
                                    setState({ ...state, isLoading: false });
                                });
                        }
                        else {
                            Error("Failed to process, please try again");
                            setState({ ...state, isLoading: false });
                        }
                    }
                    else {
                        const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                        Error(msg);
                        setState({ ...state, isLoading: false });
                    }
                })
                .catch(err => {
                    Error(err.toString());
                    setState({ ...state, isLoading: false });
                });
        }
    }

    const handleChange = (e) => {
        if (isEmpty(e.target)) {
            setState({ ...state, "_state": e.value });
        }
        else {
            setState({ ...state, [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value });
        }
    }

    const handleChangeAlternateState = (e) => {
        setState({ ...state, "alternate_state": e.value });
    }

    const isLoading = state.isLoading;

    return (
        <div className='school-admin-portal'>
            <div className='admin-portal-container px-md-5 sign-up-capsule'>
                <div className='admin-portal-title'>Group Administration Portal</div>
                {
                    isLoading ?
                        <Loader type="Triangle" color="#6d4c96" height={100} width={100} className="text-center" />
                        :
                        <Form noValidate onSubmit={handleFormSubmit}>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="6">
                                    <Form.Control name="first_name" className="mb-xs-3" type="text" placeholder="First" onChange={handleChange} value={state.first_name} value={state.first_name} />
                                    <Form.Text className="text-danger">{errorMsg.firtNameErrorMsg}</Form.Text>
                                </Col>
                                <Col sm="6">
                                    <Form.Control name="last_name" type="text" placeholder="Last" onChange={handleChange} value={state.last_name} />
                                    <Form.Text className="text-danger">{errorMsg.lastNameErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="phone" type="text" placeholder="Phone" maxLength={10} onChange={handleChange} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9 ]/g, '')} value={state.phone} />
                                    <Form.Text className="text-danger">{errorMsg.phoneErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="email" type="email" placeholder="Email Address" onChange={handleChange} value={state.email} />
                                    <Form.Text className="text-danger">{errorMsg.emailErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="address" type="text" placeholder="Address" onChange={handleChange} value={state.address} />
                                    <Form.Text className="text-danger">{errorMsg.addressErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="6">
                                    <Form.Control name="city" className="mb-xs-3" type="text" placeholder="City" onChange={handleChange} value={state.city} />
                                    <Form.Text className="text-danger">{errorMsg.cityErrorMsg}</Form.Text>
                                </Col>
                                <Col sm="3">
                                    <Select name="_state" className="mb-xs-3 custom-dropdown" placeholder="State" defaultValue={state._state} options={state.statelist} onChange={handleChange} isSearchable={true} />
                                    <Form.Text className="text-danger">{errorMsg.stateErrorMsg}</Form.Text>
                                </Col>
                                <Col sm="3">
                                    <Form.Control name="zip" type="text" placeholder="Zip" maxLength={5} onChange={handleChange} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9]/g, '')} value={state.zip} />
                                    <Form.Text className="text-danger">{errorMsg.zipErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pb-5 text-start">
                                <Col sm="3" />
                                <Col sm="6" className="d-flex align-items-center justify-content-evenly">
                                    <div>
                                        <Form.Label className="d-block mb-0">Want to be a driver?</Form.Label>
                                        <Form.Text className="text-muted">
                                            You can always choose later
                                        </Form.Text>
                                    </div>
                                    <Form.Check name="is_driver" type="checkbox" className="display-5 is_driver_ckb" onChange={handleChange} checked={state.is_driver} />
                                </Col>
                                <Col sm="3" />
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pt-3 border-top border-1 border-dark">
                                <Col sm="12">
                                    <Form.Label>Some additional information</Form.Label>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="alternate_address" type="text" placeholder="Alternate Address (optional)" onChange={handleChange} value={state.alternate_address} />
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="8">
                                    <Form.Control name="alternate_city" className="mb-xs-3" type="text" placeholder="City (optional)" onChange={handleChange} value={state.alternate_city} />
                                </Col>
                                <Col sm="4">
                                    <Select name="alternate_state" className="mb-xs-3 custom-dropdown" placeholder="State (optional)" defaultValue={state.alternate_state} options={state.statelist} onChange={handleChangeAlternateState} isSearchable={true} />
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="alternate_phone" type="text" placeholder="Alternate Phone (optional)" maxLength={10} onChange={handleChange} onInput={(e) => e.target.value = e.target.value.replace(/[^0-9]/g, '')} value={state.alternate_phone} />
                                    <Form.Text className="text-danger">{errorMsg.alternatePhoneErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pb-5">
                                <Col sm="12">
                                    <Form.Control name="alternate_email" type="email" placeholder="Alternate Email Address (optional)" onChange={handleChange} value={state.alternate_email} />
                                    <Form.Text className="text-danger">{errorMsg.alternateEmailErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 pt-3 border-top border-1 border-dark">
                                <Col sm="12">
                                    <Form.Label>Choose a password</Form.Label>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="password" type="password" placeholder="Password..." minLength={6} maxLength={20} onChange={handleChange} value={state.password} />
                                    <Form.Text className="text-danger">{errorMsg.passwordErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Form.Control name="confirm_password" type="password" placeholder="Confirm Password..." minLength={6} maxLength={20} onChange={handleChange} value={state.confirm_password} />
                                    <Form.Text className="text-danger">{errorMsg.confirmPasswordErrorMsg}</Form.Text>
                                </Col>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Col sm="12">
                                    <Button variant="primary col-sm-12" type="button" onClick={() => window.open("termsofservice", "_blank")}>Terms of Service</Button>
                                </Col>
                                <Form.Text className="text-muted">
                                    Our platform puts parents in charge of who gets to join their carpooling pods. Please take care in who you allow to join your pods.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5 py-4 text-start">
                                <Col sm="3" />
                                <Col sm="6" className="d-flex align-items-center justify-content-evenly">
                                    <div>
                                        <Form.Label className="d-block mb-0">I agree to the terms of service</Form.Label>
                                        <Form.Text className="text-muted">
                                            By submitting this form, you are agreeing to let My School Carpool...
                                        </Form.Text>
                                    </div>
                                    <Form.Check name="is_terms_service_agreed" type="checkbox" className="display-5" onChange={handleChange} checked={state.is_terms_service_agreed} />
                                </Col>
                                <Col sm="3" />
                            </Form.Group>

                            <Form.Group as={Row} className="my-4 mx-md-5">
                                <Form.Text className="text-muted pb-3">
                                    You can complete your driver profile and add children from our mobile app.
                                </Form.Text>
                                <Col sm="12">
                                    <Button variant="primary col-sm-12" type="submit" disabled={!state.is_terms_service_agreed} >
                                        Join Now
                                    </Button>
                                </Col>
                            </Form.Group>
                        </Form>
                }
            </div>
        </div>
    );

}

export default withRouter(SignUp);