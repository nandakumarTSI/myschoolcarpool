import React, { Component } from 'react';
//import './style.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { isEmpty } from 'lodash';
import Loader from "react-loader-spinner";

import config from "../../config";
import { Error, Success } from "../ToastMessage/index";

class UserDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        }
    }

    handleViewProfile = (parent_id, driver_id) => {
        localStorage.setItem("parent_id", parent_id);
        localStorage.setItem("driver_id", driver_id);
        this.props.history.push({
            pathname: "/viewprofile",
            state: {
                parent_id: parent_id,
                driver_id: driver_id
            }
        });
    }

    handleSendEmail = () => {
        window.open("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin");
    }

    handleApproveAccount = () => {
        this.setState({ isLoading: true });
        fetch(`${config.apiURL}/group/updateGroupMemberStatusByGroupAdminId/${parseInt(localStorage.getItem("group_id"))}`, {
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "user_id": localStorage.getItem("user_id"),
                'status': "4"
            })
        })
            .then((response) => response.json())
            .then(response => {
                if (response.status === true) {
                    if (isEmpty(response.body)) {
                        Error("No Registered Vehicles Found");
                    }
                    else {
                        Success("Account Approved Successfull");
                    }
                }
                else {
                    const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                    Error(msg);
                }
            })
            .catch(err => {
                Error(err.toString());
            })
            .finally(x => {
                this.setState({ isLoading: false });
            });
    }

    render() {
        const isLoading = this.state.isLoading;
        const warningMessage = 'No New Accounts Pending Approval';
        const { details } = this.props;

        if (isEmpty(details) || isEmpty(details.filter(x => x.request_status === 3))) {
            return <div className='warn-msg'>{warningMessage}</div>
        }

        return (
            <div className='new-acc-pending-info'>
                {isLoading ?
                    <Loader type="Triangle" color="#6d4c96" height={100} width={100} className="text-center" />
                    :
                    details.filter(x => x.request_status === 3).map((res) => {
                        return <div key={res.id} className='upcoming-rides-info'>

                            <div className='parent-driver-info'>
                                <img src={res.picture} alt='Avatar' className='parent-driver-img'></img>
                                <div className='parent-driver-details'>
                                    <div className='parent-driver-name'>{res.first_name} {res.last_name}</div>
                                    <div className='parent-driver-status'>Parent Driver</div>
                                    <div className='parent-driver-btns'>
                                        <Button variant='default' className='view-profile-btn' onClick={() => this.handleViewProfile(res.id, res.driver_id)}>View Profile</Button>
                                        <Button variant='default' className='send-email-btn' onClick={() => this.handleSendEmail()}>Send Email</Button>
                                    </div>
                                </div>
                            </div>

                            {!isEmpty(res.students) && <div className='assigned-children'>
                                <div className='assigned-children-title-container'>
                                    <div className='down-arrow-symbol'></div>
                                    <div className='assigned-children-title'>Children assigned to this ride:</div>
                                </div>
                                <div className='parent-driver-child-info-ctn'>
                                    {res.students.map((child) => {
                                        return <div key={child.id} className='parent-driver-child-info'>
                                            <img src={child.picture} alt='Avatar' className='parent-driver-child-img'></img>
                                            <div className='parent-driver-child-details'>
                                                <div className='parent-driver-child-name'>{child.name}</div>
                                                <div className='parent-driver-child-status'>Child</div>
                                            </div>
                                        </div>
                                    })}
                                </div>
                            </div>}
                            <Button variant='default' className='acc-status' onClick={() => this.handleApproveAccount()}>{'Approve Account'}</Button>
                            <div className='upcoming-rides-end-hr'></div>
                        </div>
                    })
                }
            </div>
        );
    }
}

export default withRouter(connect()(UserDetails));