import React, { Component } from 'react';
//import './style.css';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';



class ForgotPasswordResponse extends Component {


    handleReturnToLogin = () => {
        this.props.history.push('/login');
    }

    render() {
        const msg = this.props.user.message;
        return (
            <div className='forgot-password-response-msg'>
                <div className='forgot-password-title'>{msg}</div>
                <button onClick={this.handleReturnToLogin}>Return to Login</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { resetpassword } = state;
    return {
        user: resetpassword.user
    }
}

export default withRouter(connect(mapStateToProps, {})(ForgotPasswordResponse));