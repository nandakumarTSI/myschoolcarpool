import React, { Component } from 'react';
//import './style.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { isEmpty } from 'lodash';
import { Button } from 'react-bootstrap';

import RecentHistory from '../RecentHistory/index';
import { createGetRideScheduleRequest } from '../../actions/GetRideScheduleByGroupIdAction';
import Loader from '../Loader/index';
import moment from 'moment';
import momentTimeZone from 'moment-timezone';

class RideSchedule extends Component {

    constructor(props) {
        super(props);

        this.state = {
            errorMsg: '',
            isLoading: true
        };
    }

    componentDidMount = () => {
        this.props.createGetRideScheduleRequest(momentTimeZone.tz.guess(true))
            .then(
                res => {
                },
                err => {
                    this.setState({
                        errorMsg: err.toString()
                    });
                }
            )
            .then(x => {
                this.setState({ isLoading: false });
            });
    }

    render() {
        const isLoading = this.state.isLoading;
        if (isLoading) {
            return <Loader />
        }

        const { data } = this.props;

        let errorMsg = 'No Upcoming Rides';
        if (!isEmpty(data)) {
            if (!data.status) {
                errorMsg = (!isEmpty(data.error_code) ? "Error Code: " + data.error_code : "") + " " + (!isEmpty(data.message) ? "Message: " + data.message : "");
            }
            else if (isEmpty(data.body) || isEmpty(data.body.upcoming_rides)) {
                errorMsg = 'No Upcoming Rides';
            }
            else {
                errorMsg = '';
            }
        }

        return (
            <div className='container-recent-history-div'>
                <div className='row'>
                    <RecentHistory />
                    <div className='col-md-9 p-0'>
                        <div className='capsule'>
                            <div className='title-color'>RIDE SCHEDULE</div>
                            <div className='account-summary'>
                                <div className='account-summary-title'>Upcoming Rides</div>
                                <div className='account-summary-hr'></div>
                                {
                                    (isEmpty(errorMsg))
                                        ?
                                        <UpComingRides upcomingRides={data.body.upcoming_rides} history={this.props.history} />
                                        :
                                        <div className='warn-msg'>{!isEmpty(this.state.errorMsg) ? this.state.errorMsg : errorMsg}</div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { login, getGroupRidesAll } = state;

    return {
        user: login.user,
        data: getGroupRidesAll.data
    }
}
export default withRouter(connect(mapStateToProps, { createGetRideScheduleRequest })(RideSchedule));


const UpComingRides = (data) => {

    const upcomingRides = data.upcomingRides;

    const handleViewProfile = (parent_id, driver_id) => {
        localStorage.setItem("parent_id", parent_id);
        localStorage.setItem("driver_id", driver_id);
        data.history.push({
            pathname: "/viewprofile",
            state: {
                parent_id: parent_id,
                driver_id: driver_id
            }
        });
    }

    const handleSendEmail = () => {
        window.open("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin");
    }

    return (
        <div className='new-acc-pending-info'>
            {
                upcomingRides.map(rides => {
                    const parent_details = rides.group_ride_schedule_details.group_ride_schedule_parents;
                    const children_details = rides.group_ride_schedule_details.group_ride_schedule_childrens;

                    if (isEmpty(upcomingRides.filter(x => x.group_ride_schedule_details.group_ride_schedule_parents.length > 0))) {
                        return <div key={rides.group_ride_schedule_details.id} className='warn-msg'>{'No Upcoming Rides'}</div>
                    }

                    return <div key={rides.group_ride_schedule_details.id}>
                        {parent_details.filter(x => x.is_driver === 1).map((res) => {
                            return <div key={res.id} className='upcoming-rides-info'>
                                <div className='upcoming-date-time'>{moment(rides.group_ride_schedule_details.schedule_datetime).format('MMMM DD YYYY - h:mm a')}</div>
                                <div className='parent-driver-info'>
                                    <img src={res.parent_details.profile_picture} alt='Avatar' className='parent-driver-img'></img>
                                    <div className='parent-driver-details'>
                                        <div className='parent-driver-name'>{res.parent_details.first_name} {res.parent_details.last_name}</div>
                                        <div className='parent-driver-status'>Parent Driver</div>
                                        <div className='parent-driver-btns'>
                                            <Button variant='default' className='view-profile-btn' onClick={() => handleViewProfile(res.parent_id, res.parent_id)}>View Profile</Button>
                                            <Button variant='default' className='send-email-btn' onClick={() => handleSendEmail()}>Send Email</Button>
                                        </div>
                                    </div>
                                </div>

                                {!isEmpty(children_details.filter(x => x.parent_id === res.parent_id)) && <div className='assigned-children'>
                                    <div className='assigned-children-title-container'>
                                        <div className='down-arrow-symbol'></div>
                                        <div className='assigned-children-title'>Children assigned to this ride:</div>
                                    </div>
                                    <div className='parent-driver-child-info-ctn'>
                                        {
                                            children_details.filter(x => x.parent_id === res.parent_id).map((child) => {
                                                return child.students.filter(y => y.id === child.children_id).map(c => {
                                                    return <div key={c.id} className='parent-driver-child-info'>
                                                        <img src={c.picture} alt='Avatar' className='parent-driver-child-img'></img>
                                                        <div className='parent-driver-child-details'>
                                                            <div className='parent-driver-child-name'>{c.first_name} {c.last_name}</div>
                                                            <div className='parent-driver-child-status'>Child of First Last</div>
                                                        </div>
                                                    </div>
                                                })
                                            })
                                        }
                                    </div>
                                </div>}
                                <div className='upcoming-rides-end-hr'></div>
                            </div>
                        })}
                    </div>
                })
            }
        </div>
    );
}