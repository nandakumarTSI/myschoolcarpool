//import './style.css';
import React, { Component } from "react";
import { connect } from 'react-redux';
import { Button, Form } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { isEmpty, isNull } from 'lodash';

import RecentHistory from "../RecentHistory/index";
import config from '../../config';
import { Error, Warning } from '../ToastMessage/index';
import Loader from '../Loader/index';
import LeftArrow from '../../assets/images/left-arrow.png';
import RightArrow from '../../assets/images/right-arrow.png';


class ViewProfile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            picture: '',
            email: '',
            first_name: '',
            last_name: '',
            address: '',
            city: '',
            state: '',
            zip: '',
            alternate_address: '',
            alternate_city: '',
            alternate_state: '',
            alternate_zip: '',
            phone: '',
            alternate_phone: '',
            isDisable: true,

            vehicle_id: 0,
            vehicle_make: '',
            vehicle_model: '',
            vehicle_color: '',
            vehicle_year: '',
            available_seats: '',

            registred_vehicles_inx: 0,
            registred_vehicles: [
                {
                    id: 0,
                    vehicle_make: '',
                    vehicle_model: '',
                    vehicle_color: '',
                    vehicle_year: '',
                    available_seats: '',
                    insurance_card: '',
                    vehicle_registration: '',
                    inspection_report: '',
                }
            ]
        }

        this.onChange = this.onChange.bind(this);
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    componentDidMount() {

        fetch(`${config.apiURL}/viewParent?id=${localStorage.getItem("parent_id")}`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then(res => {
                let address = null, city = null, state = null, zip = null, alternate_address = null, alternate_city = null, alternate_state = null, alternate_zip = null, phone = null, alternate_phone = null;
                let registred_vehicles = [];

                if (res.status === true) {

                    for (let i = 0; i < res.body.user_location_details.length; i++) {
                        if (res.body.user_location_details[i].is_default === "1") {
                            address = res.body.user_location_details[i].address;
                            city = res.body.user_location_details[i].city;
                            state = res.body.user_location_details[i].state;
                            zip = res.body.user_location_details[i].zip;
                            phone = res.body.user_location_details[i].phone;
                        }
                        else {
                            alternate_address = res.body.user_location_details[i].address;
                            alternate_city = res.body.user_location_details[i].city;
                            alternate_state = res.body.user_location_details[i].state;
                            alternate_zip = res.body.user_location_details[i].zip;
                            alternate_phone = res.body.user_location_details[i].phone;
                        }
                    }

                    this.setState({ isLoading: true });

                    fetch(`${config.apiURL}/listVehicles?driver_id=${localStorage.getItem("driver_id")}`, {
                        method: 'get',
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('token'),
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then((response) => response.json())
                        .then(response => {
                            let registredVehicles = {};

                            if (response.status === true) {
                                if (response.body.length > 0) {
                                    for (let j = 0; j < response.body.length; j++) {
                                        registredVehicles = {};
                                        registredVehicles.id = response.body[j].id;
                                        registredVehicles.vehicle_make = response.body[j].vehicle_make;
                                        registredVehicles.vehicle_model = response.body[j].vehicle_model;
                                        registredVehicles.vehicle_color = response.body[j].vehicle_color;
                                        registredVehicles.vehicle_year = response.body[j].vehicle_year;
                                        registredVehicles.available_seats = response.body[j].available_seats;

                                        registredVehicles.insurance_card = response.body[j].insurance_card;
                                        registredVehicles.vehicle_registration = response.body[j].vehicle_registration;
                                        registredVehicles.inspection_report = response.body[j].inspection_report;

                                        registred_vehicles.push(registredVehicles);
                                    }
                                }
                                else {
                                    Warning("No Registered Vehicles Found");
                                }
                            }
                            else {
                                const msg = (!isEmpty(response.error_code) ? "Error Code: " + response.error_code : "") + " " + (!isEmpty(response.message) ? "Message: " + response.message : "");
                                Warning(msg);
                            }

                            this.setState({
                                picture: res.body.profile_picture,
                                email: res.body.email,
                                first_name: isNull(res.body.first_name) ? "" : res.body.first_name,
                                last_name: isNull(res.body.last_name) ? "" : res.body.last_name,

                                address: isNull(address) ? "" : address,
                                city: isNull(city) ? "" : city,
                                state: isNull(state) ? "" : state,
                                zip: isNull(zip) ? "" : zip,
                                phone: isNull(phone) ? "" : phone,

                                alternate_address: isNull(alternate_address) ? "" : alternate_address,
                                alternate_city: isNull(alternate_city) ? "" : alternate_city,
                                alternate_state: isNull(alternate_state) ? "" : alternate_state,
                                alternate_zip: isNull(alternate_zip) ? "" : alternate_zip,
                                alternate_phone: isNull(alternate_phone) ? "" : alternate_phone,

                                isDisable: true,

                                vehicle_id: registred_vehicles.length > 0 ? registred_vehicles[0].id : 0,
                                vehicle_make: registred_vehicles.length > 0 ? registred_vehicles[0].vehicle_make : '',
                                vehicle_model: registred_vehicles.length > 0 ? registred_vehicles[0].vehicle_model : '',
                                vehicle_color: registred_vehicles.length > 0 ? registred_vehicles[0].vehicle_color : '',
                                vehicle_year: registred_vehicles.length > 0 ? registred_vehicles[0].vehicle_year : '',
                                available_seats: registred_vehicles.length > 0 ? registred_vehicles[0].available_seats : '',

                                insurance_card: registred_vehicles.length > 0 ? registred_vehicles[0].insurance_card : '',
                                vehicle_registration: registred_vehicles.length > 0 ? registred_vehicles[0].vehicle_registration : '',
                                inspection_report: registred_vehicles.length > 0 ? registred_vehicles[0].inspection_report : '',

                                registred_vehicles: registred_vehicles
                            });

                        })
                        .catch(err => {
                            Error(err.toString());
                        })
                        .finally(x => {
                            this.setState({ isLoading: false });
                        });

                } else {
                    const msg = (!isEmpty(res.error_code) ? "Error Code: " + res.error_code : "") + " " + (!isEmpty(res.message) ? "Message: " + res.message : "");
                    Warning(msg);
                }
            })
            .catch(err => {
                Error(err.toString());
            })
            .finally(x => {
                this.setState({ isLoading: false });
            });
    }

    handlePreviousRegisteredVehicles = () => {
        const i = this.state.registred_vehicles_inx - 1;
        if (i >= 0) {
            this.setState({
                registred_vehicles: [...this.state.registred_vehicles],
                vehicle_id: this.state.registred_vehicles[i].id,
                vehicle_make: this.state.registred_vehicles[i].vehicle_make,
                vehicle_model: this.state.registred_vehicles[i].vehicle_model,
                vehicle_color: this.state.registred_vehicles[i].vehicle_color,
                vehicle_year: this.state.registred_vehicles[i].vehicle_year,
                available_seats: this.state.registred_vehicles[i].available_seats,

                insurance_card: this.state.registred_vehicles[i].insurance_card,
                vehicle_registration: this.state.registred_vehicles[i].vehicle_registration,
                inspection_report: this.state.registred_vehicles[i].inspection_report,

                registred_vehicles_inx: i,
            });
        }
    }

    handleNextRegisteredVehicles = () => {
        const i = this.state.registred_vehicles_inx + 1;
        if (this.state.registred_vehicles.length > i) {
            this.setState({
                registred_vehicles: [...this.state.registred_vehicles],
                vehicle_id: this.state.registred_vehicles[i].id,
                vehicle_make: this.state.registred_vehicles[i].vehicle_make,
                vehicle_model: this.state.registred_vehicles[i].vehicle_model,
                vehicle_color: this.state.registred_vehicles[i].vehicle_color,
                vehicle_year: this.state.registred_vehicles[i].vehicle_year,
                available_seats: this.state.registred_vehicles[i].available_seats,

                insurance_card: this.state.registred_vehicles[i].insurance_card,
                vehicle_registration: this.state.registred_vehicles[i].vehicle_registration,
                inspection_report: this.state.registred_vehicles[i].inspection_report,

                registred_vehicles_inx: i,
            });
        }
    }

    handleFiles = (entityType) => {
        let queryStr = (entityType === "driver" ? `entity=driver&file_name=${this.state.insurance_card}` : (entityType === "vehicle_Registration" ? `entity=vehicle&file_name=${this.state.vehicle_registration}` : `entity=vehicle&file_name=${this.state.inspection_report}`));
        this.setState({ isLoading: true });
        fetch(`${config.apiURL}/fileStream?${queryStr}`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Content-Type': 'application/octet-stream',
                'cache-control': 'no-cache'
            }
        })
            .then(response => {
                const contentType = response.headers.get("content-type");
                if (contentType && contentType.indexOf("application/octet-stream") !== -1) {
                    return response.blob().then(data => {
                        const fileName = (entityType === "driver" ? this.state.insurance_card : (entityType === "vehicle_Registration" ? this.state.vehicle_registration : this.state.inspection_report));
                        const blob = new Blob([data], { type: 'application/octet-stream' });
                        const downloadUrl = URL.createObjectURL(blob);
                        let a = document.createElement('a');
                        a.style.display = 'none';
                        a.href = downloadUrl;
                        a.download = fileName;
                        document.body.appendChild(a);
                        a.click();
                        setTimeout(() => {
                            window.URL.revokeObjectURL(downloadUrl);
                            document.body.removeChild(a);
                        }, 0);
                    });
                }
                else {
                    if (contentType && contentType.indexOf("application/json") !== -1) {
                        return response.json().then(res => {
                            if (!res.status) {
                                Warning("File Not Found");
                            }
                        });
                    }

                }
            })
            .catch(err => {
                Error(err.toString());
            })
            .finally(x => {
                this.setState({ isLoading: false });
            });
    }

    render() {
        const isLoading = this.state.isLoading;
        if (isLoading) {
            return <Loader />
        }

        const { picture, email, first_name, last_name, address, city, state, zip, alternate_address, alternate_city, alternate_state, alternate_zip, phone, alternate_phone, isDisable, vehicle_make, vehicle_model, vehicle_color, vehicle_year, available_seats, registred_vehicles, registred_vehicles_inx } = this.state;
        const indexPage = registred_vehicles.length === 0 ? 0 : registred_vehicles_inx + 1;
        return (
            <div className='container-recent-history-div'>
                <div className='row'>
                    <RecentHistory />
                    <div className='col-md-9 p-0'>
                        <div className='capsule parent-profile'>
                            <div className='parent-profile-pic'>
                                <div className='title-color'>PARENT PROFILE</div>
                                <img className='parent-profile-img' src={picture} alt='Profile Avatar'></img>
                                <Button disabled={isDisable}>Edit Driver Profile</Button>
                            </div>
                            <div className='parent-email'>{email}</div>
                            <div className='driver-status'>
                                <span className='driver-status-title'>Driver Status:</span>
                                <div className='driver-status-btn-ctrls'>
                                    <Button className='active-btn' disabled={isDisable}>ACTIVE</Button>
                                    <div className='non-driver'>
                                        <span className='driver-status-note'>Note: or</span>
                                        <Button className='non-active-btn' disabled={isDisable}>NON-DRIVER</Button>
                                    </div>
                                </div>
                            </div>
                            <div className='parent-info'>
                                <div className='parent-info-title'>Parent Info</div>
                                <Form>
                                    <Form.Control type='text' name='first_name' onChange={this.onChange} placeholder='First Name' disabled={isDisable} value={first_name} />
                                    <Form.Control type='text' name='last_name' onChange={this.onChange} placeholder='Last Name' disabled={isDisable} value={last_name} />
                                    <Form.Control type='text' name='address' onChange={this.onChange} placeholder='Address' disabled={isDisable} value={address} />
                                    <Form.Control type='text' name='city' onChange={this.onChange} placeholder='City' disabled={isDisable} value={city} />
                                    <Form.Control type='text' name='state' onChange={this.onChange} placeholder='State' disabled={isDisable} value={state} />
                                    <Form.Control type='text' name='zip' onChange={this.onChange} placeholder='Zip' disabled={isDisable} value={zip} />
                                    <Form.Control type='text' name='alternate_address' onChange={this.onChange} placeholder='Alternate Address' disabled={isDisable} value={alternate_address} />
                                    <Form.Control type='text' name='alternate_city' onChange={this.onChange} placeholder='City' disabled={isDisable} value={alternate_city} />
                                    <Form.Control type='text' name='alternate_state' onChange={this.onChange} placeholder='State' disabled={isDisable} value={alternate_state} />
                                    <Form.Control type='text' name='alternate_zip' onChange={this.onChange} placeholder='Zip' disabled={isDisable} value={alternate_zip} />
                                    <Form.Control type='text' name='phone' onChange={this.onChange} placeholder='Phone' disabled={isDisable} value={phone} />
                                    <Form.Control type='text' name='alternate_phone' onChange={this.onChange} placeholder='Alternate Phone' disabled={isDisable} value={alternate_phone} />
                                </Form>
                                <Button className='view-drivers-license-btn' onClick={() => this.handleFiles("driver")}>View Drivers License</Button>
                            </div>
                            <div className='registered-vehicles-container'>
                                <div className='registered-vehicles-title'>Registered Vehicle(s)</div>
                                <div className='registered-vehicles-nav'>
                                    <Button className='registered-vehicles-left-arrow' onClick={() => this.handlePreviousRegisteredVehicles()}><img src={LeftArrow} alt="LeftArrow" width={30} height={30} /></Button>
                                    <div className='registered-vehicles-current-index'>Vehicle {indexPage} of {registred_vehicles.length}</div>
                                    <Button className='registered-vehicles-right-arrow' onClick={() => this.handleNextRegisteredVehicles()}><img src={RightArrow} alt="RightArrow" width={30} height={30} /></Button>
                                </div>

                                <form className='registered-vehicles-form'>
                                    <input type='text' className='form-input-width' name='vehicle_make' onChange={this.onChange} placeholder='Vehicle Make' disabled={isDisable} value={vehicle_make} />
                                    <input type='text' className='form-input-width' name='vehicle_model' onChange={this.onChange} placeholder='Vehicle Model' disabled={isDisable} value={vehicle_model} />
                                    <input type='text' className='form-input-width' name='vehicle_color' onChange={this.onChange} placeholder='Vehicle Color' disabled={isDisable} value={vehicle_color} />
                                    <input type='text' className='form-input-width' name='vehicle_year' onChange={this.onChange} placeholder='Vehicle Year' disabled={isDisable} value={vehicle_year} />
                                    <input type='text' className='form-input-width' name='available_seats' onChange={this.onChange} placeholder='Available Seats' disabled={isDisable} value={available_seats} />
                                </form>

                                <Button className='view-vehicle-registration-btn' onClick={() => this.handleFiles("vehicle_Registration")}>View Vehicle Registration</Button>
                                <Button className='view-vehicle-inspection-report-btn' onClick={() => this.handleFiles("vehicle_Inspection_Report")}>View Vehicle Inspection Report</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default withRouter(connect()(ViewProfile));