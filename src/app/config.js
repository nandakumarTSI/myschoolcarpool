
const ENVIRONMENT = "DEV";

const configURLs = {
  apiURL: 'https://dev.myschoolcarpool.net/api/v1',
  //apiURL: 'https://myschoolcarpool.tender.net/api/v1',
  //apiURL: 'https://stage.myschoolcarpool.net/api/v1'
}

const config = {
  ...configURLs,
  maxRetryCount: 2,
  authExpiryTime: 60000 * 5, //In Millisecond ~5 Minutes
  authKey: 'mopo_auth_key',
  environment: ENVIRONMENT,
};

export default config;