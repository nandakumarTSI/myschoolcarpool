import { GET_NEW_ACCOUNT_PENDING_APPROVAL } from './types';
import config from '../config';

export function getNewAccountPendingApproval(data) {
    return {
        type: GET_NEW_ACCOUNT_PENDING_APPROVAL,
        data
    };
}

export function createGetNewAccountPendingApprovalRequest(group_id) {
    return dispatch =>
        fetch(`${config.apiURL}/group/groupDetails`, {
            method: 'post',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "group_id": group_id,
                "is_web": true
            })
        })
            .then((response) => response.json())
            .then(res => dispatch(getNewAccountPendingApproval(res)));
}
