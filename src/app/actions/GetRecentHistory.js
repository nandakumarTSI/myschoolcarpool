import { GET_RECENT_HISTORY } from './types';
import config from '../config';

export function getRecentHistory(data) {
    return {
        type: GET_RECENT_HISTORY,
        data
    };
}

export function createGetRecentHistoryRequest() {
    return dispatch =>
        fetch(`${config.apiURL}/user/notifications`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then((response) => response.json())
            .then(res => dispatch(getRecentHistory(res)));
}
