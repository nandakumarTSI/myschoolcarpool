import { GET_GROUP_DETAILS_BY_USER_ID } from './types';
import config from '../config';

export function getGroupDetails(data) {
    return {
        type: GET_GROUP_DETAILS_BY_USER_ID,
        data
    };
}

export function createGetGroupDetailsRequest() {
    return dispatch =>
        fetch(`${config.apiURL}/group/allGroupsListByUserId`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then((response) => response.json())
            .then(res => dispatch(getGroupDetails(res)));
}
