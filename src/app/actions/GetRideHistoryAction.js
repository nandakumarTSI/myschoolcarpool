import { GET_RIDE_HISTORY } from './types';
import config from '../config';

export function getRideHistory(data) {
    return {
        type: GET_RIDE_HISTORY,
        data
    };
}

export function createGetRideHistoryRequest(currentTimeZone) {
    return dispatch =>
        fetch(`${config.apiURL}/group/groupRidesHistory?user_current_timezonename=${currentTimeZone}`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then((response) => response.json())
            .then(res => dispatch(getRideHistory(res)));
}
