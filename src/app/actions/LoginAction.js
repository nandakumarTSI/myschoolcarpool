import { SET_CURRENT_USER } from './types';
import config from '../config';

export function loginUser(user) {
    return {
        type: SET_CURRENT_USER,
        user,
    };
}

export function createLoginRequest(data) {

    const param = {
        email: data.email.toLowerCase(),
        password: data.password,
        device_type: data.device_type,
        device_token: data.device_token
    };

    return dispatch =>
        fetch(`${config.apiURL}/login`, {
            method: 'post',
            body: JSON.stringify(param),
            headers: {
                'content-type': 'application/json',
            },
        })
            .then((response) => response.json())
            .then(res => dispatch(loginUser(res)));
}
