import { GET_GROUP_RIDE_ALL } from './types';
import config from '../config';

export function getRideSchedule(data) {
    return {
        type: GET_GROUP_RIDE_ALL,
        data
    };
}

export function createGetRideScheduleRequest(currentTimeZone) {
    return dispatch =>
        fetch(`${config.apiURL}/group/groupRidesAll?user_current_timezonename=${currentTimeZone}`, {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then((response) => response.json())
            .then(res => dispatch(getRideSchedule(res)));
}
