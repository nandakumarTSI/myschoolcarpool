import { RESET_PASSWORD } from './types';
import config from '../config';

export function resetPassword(user) {
    return {
        type: RESET_PASSWORD,
        user,
    };
}

export function createResetPasswordRequest(data) {

    const param = {
        email: data.email.toLowerCase()
    };

    return dispatch =>
        fetch(`${config.apiURL}/reset-password`, {
            method: 'post',
            body: JSON.stringify(param),
            headers: {
                'content-type': 'application/json',
            },
        })
            .then((response) => response.json())
            .then(res => dispatch(resetPassword(res)));
}
