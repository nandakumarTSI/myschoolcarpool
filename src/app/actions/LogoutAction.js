import { LOGOUT_SUCCESS } from './types';

export function logoutUser() {
    return {
        type: LOGOUT_SUCCESS
    };
}

export function createLogoutRequest() {
    return dispatch => dispatch(logoutUser());
}